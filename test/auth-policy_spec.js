const AuthPolicy = require('aws-auth-policy');

describe('UNIT auth policy generator', () => {
  let policy = null;
  beforeEach(() => {
    const apiOptions = {
      awsAccountId: 'ouraccountid',
      region: 'us-west-2',
      restApiId: 'theapiid(notname)',
      stage: 'theapistage'
    }
    policy = new AuthPolicy('usertoken', 'ouraccountid', apiOptions);
    policy.pathRegex = new RegExp('^[/.a-zA-Z0-9-\*_]+$');
  });
  it('will error if no statements are given', () => {
    const errorWrap = () => policy.build();
    expect(errorWrap).to.throw('No statements defined for the policy')
  });
  it('can create a specific route policy', () => {
    policy.allowMethod('GET', 'somepath/withstuff');
    const result = policy.build();
    const expectedResult = {
      principalId: 'usertoken',
      policyDocument: {
        Version: '2012-10-17',
        Statement: [
          {
            Action: 'execute-api:Invoke',
            Effect: 'Allow',
            Resource: [
              'arn:aws:execute-api:us-west-2:ouraccountid:theapiid(notname)/theapistage/GET/somepath/withstuff'
            ]
          }
        ]
      }
    };
    expect(result).to.deep.equal(expectedResult);
  });
  it('can create allow all routes policy', () => {
    policy.allowAllMethods()
    const result = policy.build();
    const expectedResult = {
      principalId: 'usertoken',
      policyDocument: {
        Version: '2012-10-17',
        Statement: [
          {
            Action: 'execute-api:Invoke',
            Effect: 'Allow',
            Resource: [
              'arn:aws:execute-api:us-west-2:ouraccountid:theapiid(notname)/theapistage/*/*'
            ]
          }
        ]
      }
    };
    expect(result).to.deep.equal(expectedResult);
  });
  it('can create deny all routes policy', () => {
    policy.denyAllMethods()
    const result = policy.build();
    const expectedResult = {
      principalId: 'usertoken',
      policyDocument: {
        Version: '2012-10-17',
        Statement: [
          {
            Action: 'execute-api:Invoke',
            Effect: 'Deny',
            Resource: [
              'arn:aws:execute-api:us-west-2:ouraccountid:theapiid(notname)/theapistage/*/*'
            ]
          }
        ]
      }
    };
    expect(result).to.deep.equal(expectedResult);
  });
  it('can accept \'_\' for resources', () => {
    policy.allowMethod('GET', 'somepath/ip_thisis200ok');
    const result = policy.build();
    const expectedResult = {
      principalId: 'usertoken',
      policyDocument: {
        Version: '2012-10-17',
        Statement: [
          {
            Action: 'execute-api:Invoke',
            Effect: 'Allow',
            Resource: [
              'arn:aws:execute-api:us-west-2:ouraccountid:theapiid(notname)/theapistage/GET/somepath/ip_thisis200ok'
            ]
          }
        ]
      }
    };
    expect(result).to.deep.equal(expectedResult);
  });
  it('can accept \'*\' for resources', () => {
    policy.allowMethod('GET', '*');
    const result = policy.build();
    const expectedResult = {
      principalId: 'usertoken',
      policyDocument: {
        Version: '2012-10-17',
        Statement: [
          {
            Action: 'execute-api:Invoke',
            Effect: 'Allow',
            Resource: [
              'arn:aws:execute-api:us-west-2:ouraccountid:theapiid(notname)/theapistage/GET/*'
            ]
          }
        ]
      }
    };
    expect(result).to.deep.equal(expectedResult);
  });
  it('can accept \'*\' for verbs', () => {
    policy.allowMethod('*', 'somepath/withstuff');
    const result = policy.build();
    const expectedResult = {
      principalId: 'usertoken',
      policyDocument: {
        Version: '2012-10-17',
        Statement: [
          {
            Action: 'execute-api:Invoke',
            Effect: 'Allow',
            Resource: [
              'arn:aws:execute-api:us-west-2:ouraccountid:theapiid(notname)/theapistage/*/somepath/withstuff'
            ]
          }
        ]
      }
    };
    expect(result).to.deep.equal(expectedResult);
  });
});
