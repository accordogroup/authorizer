// Methods to get tokens for auth0 users from their credentials
const qs = require('querystring');
const { URL } = require('url');
const rp = require('request-promise-native');
const cheerio = require('cheerio');

const getOIDCBearerToken = async ({ client_id, tenant, redirect_uri, username, password }) => {
  // see https://auth0.com/docs/flows/guides/implicit/add-login-implicit
  // for nonce and state usage. Auth0 sets own state, we must pick a nonce
  // frontend also has auth0Client header, but as far as I can tell this does nothing
  const nonce = Date.now();

  const cookieJar = rp.jar(); // fresh jar each time to avoid state pollution
 
  // because we run in simple mode, this request will auto follow from /authorize to /login
  const authResponse = await rp({
    method: 'GET',
    uri: `https://${tenant}.auth0.com/authorize`,
    qs: {
      client_id,
      response_type: 'token id_token',
      redirect_uri,
      scope: 'openid',
      type: 'login',
      nonce
    },
    resolveWithFullResponse: true,
    jar: cookieJar // theres set-cookies in response, put them in jar for later requests
  });
  // auth0 seems to set its own state, which is needed for next request
  const { state } = qs.parse(authResponse.request.uri.query);

  // this gets a token, but it is not the final token for sign in
  const loginResponse = await rp({
    method: 'POST',
    uri: `https://${tenant}.auth0.com/usernamepassword/login`,
    body: {
      client_id,
      connection: 'Username-Password-Authentication',
      nonce,
      password,
      protocol: 'oauth2',
      redirect_uri,
      response_type: 'token id_token',
      scope: 'openid',
      sso: true,
      state,
      tenant,
      type: 'login',
      username
    },
    resolveWithFullResponse: true,
    jar: cookieJar, // use cookies or perish (400)
    json: true
  });

  // response is html form that (in the browser) is auto filled and sent to auth0
  // we need to extract the form fields and make the request manually
  const dom = cheerio.load(loginResponse.body);
  const form = {
    wa: dom('input[name=wa]').attr('value'),
    wresult: dom('input[name=wresult]').attr('value'),
    wctx: dom('input[name=wctx]').attr('value')
  };

  // we want the id_token but are not interested in following subsequent callback to app url
  // hence this request is not run in simple mode to stop it following the redirect
  const tokenResponse = await rp({
    method: 'POST',
    uri: `https://${tenant}.auth0.com/login/callback`,
    form,
    jar: cookieJar, // use cookies or perish (400)
    simple: false,
    resolveWithFullResponse: true
  });

  // The token is on the redirect uri, behind hash. Totes secure.
  const hash = (new URL(tokenResponse.headers.location)).hash;
  /* eslint-disable camelcase */
  const { id_token } = qs.parse(hash.replace('#', ''));
  return id_token;
  /* eslint-enable camelcase */
};

module.exports = {
  getOIDCBearerToken
};
