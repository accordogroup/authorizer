const R = require('ramda');
const testData = require('./test_data');
const generatePolicy = require('../src/services/generatePolicy').generatePolicy;
const getApiOptions = require('../src/utils/getApiOptions').getApiOptions;
const adminToken = testData.adminToken;
const appConfig = testData.appConfig;
const expectedAdminPolicy = testData.expectedAdminPolicy;
const expectedConsultantPolicy = testData.expectedConsultantPolicy;
const expectedBatmanPolicy = testData.expectedBatmanPolicy;
const methodArn = "arn:aws:execute-api:eu-west-9:acmeco:random-application/dev/put/people";
const apiOptions = getApiOptions(methodArn);

const sampleConfig = {
  policies: [
  {role: 'roleA', resources: [{verb:'GET', resource: 'resourceA'}]},
  {role: 'roleB', resources: [{verb:'GET', resource: 'resourceB'}]}
  ]
}

describe('COMPONENT services', () => {
  describe('generatePolicy', () => {
    it('generatePolicy() allows all methods for admin role', async () => {
    
      const token = R.clone(adminToken);
      token.app_metadata.roles = ['admin', 'something else'];
      const apiOptions = getApiOptions("arn:aws:execute-api:ireland-west-1:acmeco:sample-application/dev/get/someinfo");
      
      const policy = await generatePolicy(token, apiOptions, appConfig);
      expect(policy).to.deep.equal(expectedAdminPolicy)
    });
    
    it('generatePolicy() allows endpoints registered to consultant for consultant role', async () => {
      const token = R.clone(adminToken);
      token.app_metadata.roles = ['consultant'];
      const apiOptions = getApiOptions("arn:aws:execute-api:ireland-west-1:acmeco:sample-application/dev/get/direct/1234");
      
      const policy = await generatePolicy(token, apiOptions, appConfig);
      expect(policy).to.deep.equal(expectedConsultantPolicy);
    });
    
    it('generatePolicy() restricts on subject / user id', async () => {
      
      const token = R.clone(adminToken);
      token.app_metadata.roles = ['batman'];
      const apiOptions = getApiOptions("arn:aws:execute-api:ireland-west-1:acmeco:sample-application/dev/get/someinfo");
      
      const policy = await generatePolicy(token, apiOptions, appConfig);
      
      expect(policy).to.deep.equal(expectedBatmanPolicy)
      
    });
    
    it('generatePolicy() denies all for an unknown role', async () => {
      
      const token = R.clone(adminToken);
      token.app_metadata.roles = ['joker'];
      const apiOptions = getApiOptions("arn:aws:execute-api:ireland-west-1:acmeco:sample-application/dev/get/someinfo");
      
      const policy = await generatePolicy(token, apiOptions, appConfig);
      
      expect(policy).to.deep.equal({
        "policyDocument": {
          "Statement": [
            {
              "Action": "execute-api:Invoke",
              "Effect": "Deny",
              "Resource": [
                "arn:aws:execute-api:ireland-west-1:acmeco:sample-application/dev/*/*"
              ]
            }
          ],
          "Version": "2012-10-17"
        },
        "principalId": "windowslive|a2c99bad383fa0d2"
      })
      
    });
    it('should combine new and old format roles', async () => {
      const payload = {
        sub:'person123',
        app_metadata: {
          roles: ['roleA'],
          authorization: {
            roles: ['roleB']
          }
        }
      };
      const policy = await generatePolicy(payload, apiOptions, sampleConfig);
      const statements = policy.policyDocument.Statement;
      expect(statements).to.be.ok;
      expect(statements[0]).to.deep.equal(
        {
          Action: 'execute-api:Invoke',
          Effect: 'Allow',
          Resource: [
          'arn:aws:execute-api:eu-west-9:acmeco:random-application/dev/GET/resourceA',
          'arn:aws:execute-api:eu-west-9:acmeco:random-application/dev/GET/resourceB'
          ]
        });



    });
    it('should cope with duplicate roles', async () => {
      const payload = {
        sub:'person123',
        app_metadata: {
          roles: ['roleA'],
          authorization: {
            roles: ['roleB', 'roleB']
          }
        }
      };
      const policy = await generatePolicy(payload, apiOptions, sampleConfig);
      const statements = policy.policyDocument.Statement;
      expect(statements).to.be.ok;
      expect(statements[0]).to.deep.equal(
        {
          Action: 'execute-api:Invoke',
          Effect: 'Allow',
          Resource: [
          'arn:aws:execute-api:eu-west-9:acmeco:random-application/dev/GET/resourceA',
          'arn:aws:execute-api:eu-west-9:acmeco:random-application/dev/GET/resourceB'
          ]
        });
    });
  });
});
