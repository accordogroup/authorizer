const organizations = require('../src/gateways/organizations');

describe('INTEGRATION gateways', () => {
  describe('organizations', () => {

    describe('getClientOrganisations', () => {
      it('will get the client ids for an mspId', async () => {
        const response = await organizations.getClientOrganisations('ac_bf2e5f8c-de0b-4113-b50d-8f547764cf8e', 'dev');
        expect(response).to.be.an('array').that.deep.equals([]);
      });
      it('will return empty for non-200 responses', async () => {
        const response = await organizations.getClientOrganisations('ip_ffffffff-de0b-4113-b50d-8f547764cf8e', 'dev');
        expect(response).to.deep.equal([]);
      });
    });
  });
})
