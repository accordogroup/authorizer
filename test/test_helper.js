const chai = require('chai');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const path = require('path');
const secrets = require('ssm-helper');
const request = require('request');

global.expect = chai.expect;

const customizedEnvs = {
  dev_secret: 'AQECAHh+WbHYdrY8IgMgDN+aLK1INXLpDCI7CLZ2ujU3XLecgQAAAKIwgZ8GCSqGSIb3DQEHBqCBkTCBjgIBADCBiAYJKoZIhvcNAQcBMB4GCWCGSAFlAwQBLjARBAxtyn6s10TCQxuSUQUCARCAW1iN3t8KgXPE4Dvh2pEMsiG+07lctIElaPg3fd0Z3sw4OxqKyENuedOVSycEJtI/m+zxHbcGyp+usEVTB8TVBLtvEUXLAMcI1Mb3uDSBGVfPJux5l0seuIbNVHw=',
  uat_secret: 'AQECAHh+WbHYdrY8IgMgDN+aLK1INXLpDCI7CLZ2ujU3XLecgQAAAKIwgZ8GCSqGSIb3DQEHBqCBkTCBjgIBADCBiAYJKoZIhvcNAQcBMB4GCWCGSAFlAwQBLjARBAxtyn6s10TCQxuSUQUCARCAW1iN3t8KgXPE4Dvh2pEMsiG+07lctIElaPg3fd0Z3sw4OxqKyENuedOVSycEJtI/m+zxHbcGyp+usEVTB8TVBLtvEUXLAMcI1Mb3uDSBGVfPJux5l0seuIbNVHw=',
  prod_secret: 'AQECAHh+WbHYdrY8IgMgDN+aLK1INXLpDCI7CLZ2ujU3XLecgQAAAKIwgZ8GCSqGSIb3DQEHBqCBkTCBjgIBADCBiAYJKoZIhvcNAQcBMB4GCWCGSAFlAwQBLjARBAxtyn6s10TCQxuSUQUCARCAW1iN3t8KgXPE4Dvh2pEMsiG+07lctIElaPg3fd0Z3sw4OxqKyENuedOVSycEJtI/m+zxHbcGyp+usEVTB8TVBLtvEUXLAMcI1Mb3uDSBGVfPJux5l0seuIbNVHw=',
}
const sampleSecret = "RwYAgTi6Zrgo0I78xYVNeC4Y5nzqCqHVC3TriBZFaj17fPXR3QI96ary5TnN15DT";
const cert = fs.readFileSync(path.resolve(__dirname, 'testKeys/testClient_rsa'));
const publicKey = fs.readFileSync(path.resolve(__dirname, 'testKeys/testClient_rsa.pub'));

const newEnv = Object.assign({}, process.env, customizedEnvs);


process.env = newEnv;

const createToken = (scopes, app_metadata) => {
  return jwt.sign({
    iss: 'https://funtimes.acmeco.com',
    scope: scopes,
    app_metadata: app_metadata,
    sub: "mruser|ah5f8db",
    aud: "theAccountsDepartment"
  }, sampleSecret)
}



const getAuth0Token = async () => {

  try {
    const params = await secrets.getJsonSecret('ci/accordo-authorizer/secrets');
    const options = {
      headers: {
        'Content-Type': 'application/json'
      },
      uri: 'https://dev-ipifny.auth0.com/oauth/token',
      body: params,
      json: true
    };
    return new Promise((resolve, reject) => {
      request.post(options, (err, res) => {
        if (err || res.statusCode != 200) {
          return reject(err || res.body);
        }
        return resolve(res.body.access_token)
      });
    })

  } catch (e) {
    console.error(e);
  }


}
const createRS256Token = (scopes = {}, app_metadata = {}, expiresIn = '-1h') => {
  return jwt.sign({
    scope: scopes,
    app_metadata
  },
    cert, {
    algorithm: 'RS256',
    expiresIn,
    keyid: 'fakeTokenKeyId',
    audience: 'https://api-acme.com',
    issuer: 'https://dev-ipifny.auth0.com/',
    subject: 'auth0|mrnobody'
  })
};

module.exports = {
  createToken,
  sampleSecret,
  createRS256Token,
  publicKey,
  getAuth0Token
};

