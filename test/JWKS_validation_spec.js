const validator = require('../src/utils/validateToken');
const helper = require('./test_helper');
const nock = require('nock');


const oldButValidToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlJUQXdSRGMxTmpoRVFrSkJOamxETlRJM016TkZOamxFUlRRME5UbEVOalpEUXpORU1FSkdOZyJ9.eyJodHRwczovL2FjY29yZG8uaW8vYXBwX21ldGFkYXRhIjp7Im9yZ2FuaXphdGlvbnMiOlt7Im9yZ0lkIjoiaXBfOThmMzdiYTQtYmFmNC00ZTFmLWE5YWItYzg2YWIyMmIxZWNlIiwic3RhdHVzIjoiYWN0aXZlIiwicm9sZSI6Im93bmVyIiwic3VydmV5cyI6W10sImNhbXBhaWduU291cmNlIjoiVEMgaXBpZm55IiwiY2FtcGFpZ25UeXBlSWQiOjQ3LCJjYW1wYWlnbklkIjoxMDAwNn1dfSwiaXNzIjoiaHR0cHM6Ly9kZXYtaXBpZm55LmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1YjI5NmY3NWRmMzhmZDdhMTJjNzI3MTEiLCJhdWQiOlsiaHR0cHM6Ly9kZXYtYXBpLmFjY29yZG8uaW8iLCJodHRwczovL2Rldi1pcGlmbnkuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyOTUyNjg4NCwiZXhwIjoxNTI5NjEzMjg0LCJhenAiOiJuWXRROTVKQmkzUE5yd094cmJtczY5NlJYM1pmYXFHcSIsInNjb3BlIjoib3BlbmlkIHByb2ZpbGUifQ.iEdIfeQ2w7929WP_c8ogEX37zmMv-C185MHpr3gqk3QrSJWcxyrnc3ofN97FVWEdHzl50cFo7Z2YjA_5vnEqwzdyLEpRLu9AGiM09qTZitOAbSwA1o_vWCBFf-3Vpg1ojnRNXrrxWQogrP-9gfGSZcBcKMqeiIH08-bNUgmIkhA2K8_6UX1URfsrCNfoUW31dWGNyBcoNukI3pTqaR7cQvTZ5i0w8DQG-f5Pl6o5O7oYr0EcjWeGClt643apkeIjhJ7TKq_zrJ_Y66avEOPSdGtfETZ2Z0uwV3Zn9Wmy9-zY3zFBiAreYCP2li9f0Bp3-qQpBedIEfYG60JR8xaPsA';
const verifiedKidToken = {
  'https://accordo.io/app_metadata': {
  organizations: [{
    campaignId: 10006,
    campaignSource: "TC ipifny",
    campaignTypeId: 47,
    orgId: "ip_98f37ba4-baf4-4e1f-a9ab-c86ab22b1ece",
    role: "owner",
    status: "active",
    surveys: []
  }] },
  iss: 'https://dev-ipifny.auth0.com/',
  sub: 'auth0|5b296f75df38fd7a12c72711',
  aud:
  ['https://dev-api.accordo.io',
  'https://dev-ipifny.auth0.com/userinfo'],
  iat: 1529526884,
  exp: 1529613284,
  azp: 'nYtQ95JBi3PNrwOxrbms696RX3ZfaqGq',
  scope: 'openid profile'
}

const stubEvent = {
  authorizationToken: `bearer ${oldButValidToken}`
}

const nonKidToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJodHRwczovL2FjY29yZG8uaW8vYXBwX21ldGFkYXRhIjp7InJvbGVzIjpbInVzZXIiXSwiYXV0aG9yaXphdGlvbiI6eyJncm91cHMiOltdLCJyb2xlcyI6WyJCZXRhRmVhdHVyZXMiXSwicGVybWlzc2lvbnMiOlsiU2hvd1NoYXJlT3B0aW9uIiwiUGF5bWVudCIsIlZpcnR1YWxpemF0aW9uIl19LCJvcmdhbml6YXRpb25zIjpbeyJvcmdJZCI6ImlwXzU5Njg1OTQyLTM5NTUtNDM1Yy05OGYzLTlmZWQ0MDRiMmI4MCIsInN0YXR1cyI6ImFjdGl2ZSIsInJvbGUiOiJvd25lciIsInN1cnZleXMiOlsiMTMwNTMyMjAwIl0sImNhbXBhaWduU291cmNlIjoiU0EgaXBpZm55IiwiY2FtcGFpZ25UeXBlSWQiOjQ2LCJjYW1wYWlnbklkIjoxMDAyNH1dfSwiaHR0cHM6Ly9hY2NvcmRvLmlvL3VzZXJfbWV0YWRhdGEiOnsibGFuZyI6ImVuLVVTIn0sImZhbWlseV9uYW1lIjoiQWNjb3JkbyIsIm5pY2tuYW1lIjoicHJvZHRlc3R1c2VyMTYwNDE4IiwibmFtZSI6InByb2R0ZXN0dXNlcjE2MDQxOEB0ZXN0LmNvbSIsInBpY3R1cmUiOiJodHRwczovL3MuZ3JhdmF0YXIuY29tL2F2YXRhci8yOWIyZjU3YTU2MWI0ZTA3NDFjMWY1NmU1ZWI4N2Y3Zj9zPTQ4MCZyPXBnJmQ9aHR0cHMlM0ElMkYlMkZjZG4uYXV0aDAuY29tJTJGYXZhdGFycyUyRnByLnBuZyIsInVwZGF0ZWRfYXQiOiIyMDE4LTA2LTIwVDIxOjQ0OjQ2LjA5NVoiLCJlbWFpbCI6InByb2R0ZXN0dXNlcjE2MDQxOEB0ZXN0LmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiaXNzIjoiaHR0cHM6Ly9pcGlmbnkuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfGE4MjE0MGJhNzVhMWE2NzQ2NDgzIiwiYXVkIjoiM0wzN3BXR2hKTEdManBaYkRWVEwxMXlPbEJDNWZVRXQiLCJpYXQiOjE1Mjk1MzEwODcsImV4cCI6MTUyOTU2NzA4N30.FQVLU2LmBxVAgTS-dwW5lwMGeQpE-H6JlzAdUK3OKVA';

const jwksUptions = {
  audience: 'https://dev-api.accordo.io',
  issuer: 'https://dev-ipifny.auth0.com/'
}



describe('UNIT JWKS validation', () => {
  before(() => {
    global.CI_MODE = true;
    process.env.dev_JWKS_URI = 'https://dev-ipifny.auth0.com/.well-known/jwks.json'    
  })
  after(() => {
    global.CI_MODE = false;
    process.env.JWKS_URI = '';
  })  
  describe('eventTokenHasKid', () => {

    it('should return true for kid JWT', ()=> {
      expect(validator.eventTokenHasKid(stubEvent)).to.equal(true)
    });
    it('should return false for non kid JWT', () => {
      const non = Object.assign({}, stubEvent, {authorizationToken: nonKidToken})
      expect(validator.eventTokenHasKid(non)).to.equal(false)
    })
    it('should return false garbage token', () => {
      const non = Object.assign({}, stubEvent, { authorizationToken: 'lala' })
      expect(validator.eventTokenHasKid(non)).to.equal(false)
    })
  })
  describe('verifyJWKS', () => {

    it('should return true for kid JWT', (done) => {        
      const a = 'bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlJUQXdSRGMxTmpoRVFrSkJOamxETlRJM016TkZOamxFUlRRME5UbEVOalpEUXpORU1FSkdOZyJ9.eyJodHRwczovL2FjY29yZG8uaW8vYXBwX21ldGFkYXRhIjp7Im9yZ2FuaXphdGlvbnMiOlt7Im9yZ0lkIjoiaXBfOThmMzdiYTQtYmFmNC00ZTFmLWE5YWItYzg2YWIyMmIxZWNlIiwic3RhdHVzIjoiYWN0aXZlIiwicm9sZSI6Im93bmVyIiwic3VydmV5cyI6W10sImNhbXBhaWduU291cmNlIjoiVEMgaXBpZm55IiwiY2FtcGFpZ25UeXBlSWQiOjQ3LCJjYW1wYWlnbklkIjoxMDAwNn1dfSwiaXNzIjoiaHR0cHM6Ly9kZXYtaXBpZm55LmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1YjI5NmY3NWRmMzhmZDdhMTJjNzI3MTEiLCJhdWQiOlsiaHR0cHM6Ly9kZXYtYXBpLmFjY29yZG8uaW8iLCJodHRwczovL2Rldi1pcGlmbnkuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyOTU0OTI2MSwiZXhwIjoxNTI5NjM1NjYxLCJhenAiOiJuWXRROTVKQmkzUE5yd094cmJtczY5NlJYM1pmYXFHcSIsInNjb3BlIjoib3BlbmlkIHByb2ZpbGUifQ.twAAH60N5o1tSxERyJ-ijlJhLY7aGClvQ7aYX4n8C7gUGWjraG4OHgN9V0SamghvBgE2bWCKVTa9lLB6r_THjs-yICSy2m0dkNzSd_7SFIi1eiroEptewci9YVj6v7Zn5Pil0hatQm-aumR4h_KXPlSotKUIT0IDiOr6o4ZJM55UZavcKIRQPmJnZLmSQRBYiS7sal1kGa8k_P7J0Ynsmr2y6y7UG0k6ND87_vlqQhRRM2g6BiX7JspV82L74NM2UHatO66oNurt92uixpLGRsGesAiZJX-DtWJElle02sYTfpIL0gfQdg_8f1tccqMXGVLBvgMto10S1xS4Fc_8ow';
      validator.verifyJWKS(stubEvent.authorizationToken,
        {
          ignoreExpiration: true // test token expired some time ago.
        },'dev')
      .then(decoded => {        
        expect(decoded['https://accordo.io/app_metadata']).to.deep.equal(verifiedKidToken['https://accordo.io/app_metadata'])
        done();
      })
      .catch(done)
    });
  })
  describe.skip('verify with mock', () => {   
     

    beforeEach(() => {
       nock.cleanAll();
       process.env.JWKS_URI = 'http://dev-ipifny.auth0.com/.well-known/jwks.json'
    });     
    it('should verify a newly created token', (done) => {

      nock('http://dev-ipifny.auth0.com')
        .get('/.well-known/jwks.json')
        .reply(200, { keys: [
          {
            alg: 'RS256',
            kty: 'RSA',
            use: 'sig',
            x5c: [
              'pk1'
            ],
            kid: 'fakeTokenKeyId'
            }
          ]

        })
      
      const newToken = helper.createRS256Token(null,null, '-1h');
      validator.verifyJWKS(`bearer ${newToken}`)
      .then(decoded => {
        console.log('should have failed', decoded);
        done(decoded);
      })
      .catch(err => {
        console.log(err)
        expect(err.message).to.equal({});
        done();
      })
    })
  });
})
