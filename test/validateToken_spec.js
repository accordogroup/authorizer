const validateToken = require('../src/utils/validateToken').validateToken;
const verifyJWKS = require('../src/utils/validateToken').verifyJWKS;
const aSecret = require('./test_helper').sampleSecret;

const createToken = require('./test_helper').createToken;

describe('UNIT utils', () => {
  describe('validateToken', () => {
    it('validateToken() validates a good token ok', () => {
      const token = createToken(['batman', 'superman']);
      const config = {
        secret: aSecret,
        encodeSecret: true,
        verifyOptions: {
          algorithm: 'HS256',
          issuer: 'https://funtimes.acmeco.com',
          aud: 'theAccountsDepartment'
        }
      };
      const result = validateToken(token, config)
      expect(result.scope).to.deep.equal(['batman', 'superman']);
    });

    it('validateToken() copes with a bearer prefix', () => {

      const token = createToken(['batman', 'superman']);
      const config = {
        secret: aSecret,
        encodeSecret: true,
        verifyOptions: {
          algorithm: 'HS256',
          issuer: 'https://funtimes.acmeco.com',
          aud: 'theAccountsDepartment'
        }
      };
      const result = validateToken(`bearer ${token}`, config);
      expect(result.scope).to.deep.equal(['batman', 'superman']);
    });


    it('validateToken() returns expiry message for valid old token', () => {
      const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2FjY29yZG8uYXV0aDAuY29tLyIsInN1YiI6IndpbmRvd3NsaXZlfGEyYzk5YmFkMzgzZmEwZDIiLCJhdWQiOiJSSXYwWXRpVjdVb0JBQWp6UnJpbHRoa1lFd3dlSkl5TCIsImV4cCI6MTQ1OTI3MjQwNywiaWF0IjoxNDU5MjM2NDA3fQ.Zi7pbAefnfrIwJTJ1tZbmE5AsFzVlzB8_6qDipjJMeU"; //aToken
      const config = {
        secret: aSecret,
        verifyOptions: {
          algorithm: 'HS256',
          issuer: 'https://funtimes.acmeco.com',
          aud: 'theAccountsDepartment'
        }
      };
      let err
      try {
        validateToken(token, config);
      } catch (error) {
        err = error;
      }

        expect(err.message).to.equal("jwt expired");

    });
  });

  describe('verifyJWKS', () => {
    it('returns expiry message for valid old token', async () => {
      process.env['dev_ACO_JWKS_URI'] = 'https://dev-aco.auth0.com/.well-known/jwks.json';
      const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik1rUXdOa1l4UkVKRFEwRXpPVGxDTWtRM05Ua3pNRU5HUWtNeVFqYzRNamMzUkVWRE1VUTRNUSJ9.eyJpc3MiOiJodHRwczovL2Rldi1hY28uYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVjZTFlNzcxNzUxOTE4MGZmNmFkNjA1YyIsImF1ZCI6IkFmMHN0TjE2Vlh6VXpCclBUdE5CNU1zVTN2WVVoV0UyIiwiaWF0IjoxNTU4MzE4NDk4LCJleHAiOjE1NTgzNTQ0OTgsImF0X2hhc2giOiJzZkUzTmF1R05qYVZLaDEyNS1UQjhRIiwibm9uY2UiOiJfbXNqaHp-dVQyWDZOb05HUUlqNkFTWkp5eHN3bnkwcCJ9.MtFyLYzi2UsllLZD7DWDWzqgep29Kjt2OysKXFS85PTFRtC_gLtpmHq2EnY5sl6g47mrMBECOFj9ZVH0pmF39d6T4AukjIZccuJYuOBx0rOw9wSvSH6sphqRlg_7UHgGMzz1b3XTLXdHXT4h_jgGSchL6pI9K3zfWEKAu8q55dxX1qkshqsWIFJyaF0hHkHvZkzPPzBWAGri5dTzV4pWpSeUtA275aAdgs3UvbbYsTcrHqH9ebUhF1uojF4PhIFfkc02ZUw-QBoZtXn-OVVcNd3wTtjUG0aHXgt2kbMbKMOIQUrJbM55XxDCa2o8M3_UbLUOfg0rV58JlCk58TtNTQ';
      const jwtOptions = {
        issuer: 'https://dev-aco.auth0.com/',
        audience: 'https://dev-api.accordo.io'
      };
      const env = 'dev';

      try {
        await verifyJWKS(token, jwtOptions, env);
        expect.fail('should error');
      } catch (error) {
        expect(error.name).to.equal('TokenExpiredError');
        expect(error.message).to.equal('jwt expired');
        expect(error.expiredAt).to.deep.equal(new Date('2019-05-20T12:14:58.000Z'));
      }
    });
  });
});
