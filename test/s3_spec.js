const s3 = require('../src/gateways/s3');

describe('INTEGRATION - gateways', () => {
  describe('s3', () => {
    describe('getConfigByKey', () => {
      it('can get policy for valid key', async () => {
        const result = await s3.getConfigByKey('0eiow0olh4/config.json')
        expect(result).to.have.keys(['prod', 'policies', 'version']);
      });
      it('will return undefined if policy doesnt exist', async () => {
        const result = await s3.getConfigByKey('noexisty/config.json');
        expect(result).to.equal(undefined);
      });
    });
  });
});