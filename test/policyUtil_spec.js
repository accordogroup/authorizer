const sinon = require('sinon');
const policyUtil = require('../src/utils/policyUtil');
const organizations = require('../src/gateways/organizations');
const createOIDCToken = require('./test_helper').createOIDCToken;

const sandbox = sinon.createSandbox();

describe('UNIT policyUtil', () => {
  afterEach(() => {
    sandbox.restore();
  })

  describe('getRoles', () => {
    describe('OIDC token', () => {
      let OIDCtoken;
      let result;

      beforeEach(async () => {
        OIDCtoken = {
          "name": "john.zhao+dev@accordo.com",
          "nickname": "john.zhao+dev",
          "picture": "https://s.gravatar.com/avatar/adee180e8654a8f1204f512b3a1c475c?s=480&r=pg&d=https%3A%2F%2Fcdn.auth0.com%2Favatars%2Fjo.png",
          "updated_at": "2017-09-07T23:53:49.407Z",
          "email": "john.zhao+dev@accordo.com",
          "email_verified": true,
          "iss": "https://dev-accordo.au.auth0.com/",
          "sub": "auth0|59795fd85ed87b4373f06a7f",
          "aud": "Z7ux2GGCeLDrzdXAvJNItDoAfs1Y4TX4",
          "exp": 1504864433,
          "iat": 1504828433,
          "https://optimizer.accordo.com/organizations": [{
            "orgId": "ac_123",
            "role": "partner_admin"
          }],
          "https://accordo.io/user_metadata": {},
          "https://accordo.io/app_metadata": {
            "tpid": "100015295",
            "roles": [
              "user"
            ],
            "registerUrl": "https://dev-compliance.assist.ms/#/register",
            "authorization": {
              "groups": [
                "TestGroup"
              ],
              "roles": [
                "BetaFeatures"
              ],
              "permissions": [
                "ShowChangePassword"
              ]
            },
            "temp": "false",
            "organizations": [
              {
                "orgId": "123",
                "role": "user"
              }
            ]
          }
        };
        sandbox.stub(organizations, 'getClientOrganisations').withArgs('ac_123', 'dev').resolves(['acc_456', 'acc_789']);
        result = await policyUtil.getRoles(OIDCtoken, 'dev');
      });

      it('should be able to get roles from namspaced app_metadata in OIDC token', () => {

        expect(result.userRoles).to.deep.equal(['user', 'BetaFeatures']);
      });

      it('should be able to get organizations from namspaced app_metadata and organization in OIDC token', () => {
        expect(result.orgRoles).to.deep.equal([
          {
            "orgId": "123",
            "role": "user"
          },
          {
            "orgId": "ac_123",
            "role": "partner_admin"
          },
          {
            "orgId": "acc_456",
            "role": "partner_admin"
          },
          {
            "orgId": "acc_789",
            "role": "partner_admin"
          },
        ]);
      });
    });

    describe('Old token', () => {
      let token;
      let result;

      beforeEach(() => {
        token = {
          "iss": "https://dev-accordo.au.auth0.com/",
          "sub": "auth0|59795fd85ed87b4373f06a7f",
          "aud": "BocVApIjvf84jewFfWVFH3zseVtZout2",
          "exp": 1505121291,
          "iat": 1505103291,
          "app_metadata": {
            "tpid": "100015295",
            "roles": [
              "user",
              "partner"
            ],
            "registerUrl": "https://dev-compliance.assist.ms/#/register",
            "authorization": {
              "groups": [
                "TestGroup"
              ],
              "roles": [
                "BetaFeatures"
              ],
              "permissions": [
                "ShowChangePassword"
              ]
            },
            "temp": "false",
            "organizations": [
              {
                "orgId": "123",
                "role": "user"
              }
            ]
          },
          "https://crm.accordo.com/partner": {
            "cat_user_id": 4281,
            "id": "96a9d659-6af7-4bb0-b1f8-ac0144769771"
          }
        };

      });

      it('should be able to get roles from app_metadata in old token', async () => {
        result = await policyUtil.getRoles(token);
        expect(result.userRoles).to.deep.equal(['user', 'partner', 'BetaFeatures']);
      });

      it('should be able to get organizations from app_metadata and partner id in old token', async () => {
        result = await policyUtil.getRoles(token);
        expect(result.orgRoles).to.deep.equal([
          { orgId: '123', role: 'user' }
        ]);
      });
    });
  });
});
