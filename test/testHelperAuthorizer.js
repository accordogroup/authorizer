const R = require('ramda');
const ssmHelper = require('ssm-helper');
const lambdaLocal = require('lambda-local');
const AWS = require('aws-sdk');
const auth0 = require('./auth0');

const getTokens = async () => {
  const orgownerSecrets = await ssmHelper.getJsonSecret('ci/authorizer/orgowner/secrets');
  const analystSecrets = await ssmHelper.getJsonSecret('ci/authorizer/analyst/secrets');
  const consultantSecrets = await ssmHelper.getJsonSecret('ci/authorizer/consultant/secrets');
  const adminSecrets = await ssmHelper.getJsonSecret('ci/authorizer/analyst/secrets');
  const catusersSecrets = await ssmHelper.getJsonSecret('ci/authorizer/catusers/secrets');
  const partnerSecrets = await ssmHelper.getJsonSecret('ci/authorizer/partner/secrets');
  const healthCheckSecrets = await ssmHelper.getJsonSecret('ci/authorizer/healthcheck/secrets');
  const partnerAdminSecrets = await ssmHelper.getJsonSecret('ci/authorizer/partner-admin/secrets');
  const partnerMemberSecrets = await ssmHelper.getJsonSecret('ci/authorizer/partner-member/secrets');

  const partnerAdminTokens = await Promise.all([
    auth0.getOIDCBearerToken(JSON.parse(partnerAdminSecrets.dev)),
    auth0.getOIDCBearerToken(JSON.parse(partnerAdminSecrets.uat)),
    auth0.getOIDCBearerToken(JSON.parse(partnerAdminSecrets.prod))
  ]);

  const partnerMemberTokens = await Promise.all([
    auth0.getOIDCBearerToken(JSON.parse(partnerMemberSecrets.dev)),
    auth0.getOIDCBearerToken(JSON.parse(partnerMemberSecrets.uat)),
    auth0.getOIDCBearerToken(JSON.parse(partnerMemberSecrets.prod))
  ]);

  return {
    'dev-healthcheck': healthCheckSecrets.dev,
    'uat-healthcheck': healthCheckSecrets.uat,
    'prod-healthcheck': healthCheckSecrets.prod,
    'dev-analyst': analystSecrets.dev,
    'uat-analyst': analystSecrets.uat,
    'prod-analyst': analystSecrets.prod,
    'dev-consultant': consultantSecrets.dev,
    'uat-consultant': consultantSecrets.uat,
    'prod-consultant': consultantSecrets.prod,
    'dev-orgowner': orgownerSecrets.dev,
    'uat-orgowner': orgownerSecrets.uat,
    'prod-orgowner': orgownerSecrets.prod,
    'dev-admin': adminSecrets.dev,
    'uat-admin': adminSecrets.uat,
    'prod-admin': adminSecrets.prod,
    'dev-catusers': catusersSecrets.dev,
    'uat-catusers': catusersSecrets.uat,
    'prod-catusers': catusersSecrets.prod,
    'dev-partner': partnerSecrets.dev,
    'uat-partner': partnerSecrets.uat,
    'prod-partner': partnerSecrets.prod,
    'dev-padmin': partnerAdminTokens[0],
    'uat-padmin': partnerAdminTokens[1],
    'prod-padmin': partnerAdminTokens[2],
    'dev-pmember': partnerMemberTokens[0],
    'uat-pmember': partnerMemberTokens[1],
    'prod-pmember': partnerMemberTokens[2]
  };
};

const envAndRole = [
  'dev-healthcheck',
  'uat-healthcheck',
  'prod-healthcheck',
  'dev-analyst',
  'uat-analyst',
  'prod-analyst',
  'dev-orgowner',
  'uat-orgowner',
  'prod-orgowner',
  'dev-consultant',
  'uat-consultant',
  'prod-consultant',
  'dev-catusers',
  'uat-catusers',
  'prod-catusers',
  'dev-partner',
  'uat-partner',
  'prod-partner',
  'dev-padmin',
  'uat-padmin',
  'prod-padmin',
  'dev-pmember',
  'uat-pmember',
  'prod-pmember'
];

const getTokenEnvAndRoleRegex = () => new RegExp(`(${R.join('|', envAndRole)})-input\.json`, 'gi');

const lambdaLocalExecuteEvent = event =>
  lambdaLocal.execute({
    event,
    lambdaPath: 'index',
    lambdaHandler: 'handler',
    profileName: 'default',
    timeoutMs: 30000,
    verboseLevel: 0
  });

const lambdaInvoke = R.curry((functionName, event) =>
  new AWS.Lambda({
    region: 'us-west-2'
  })
    .invoke({
      FunctionName: functionName, // name or full arn
      InvocationType: 'RequestResponse',
      Payload: JSON.stringify(event, null, 2)
    })
    .promise()
);

module.exports = {
  getTokens,
  getTokenEnvAndRoleRegex,
  lambdaLocalExecuteEvent,
  lambdaInvoke
};
