const logHelper = require('../src/utils/logHelper');

describe('UNIT logHelper', () => {
  describe('removeAuthorizationToken', () => {
    it('will replace authorizationToken if present', () => {
      const event = {
        some: 'stuff',
        authorizationToken: 'abcdefgh.execute.run.dll'
      };
      expect(logHelper.removeAuthorizationToken(event)).to.deep.equal({
        some: 'stuff',
        authorizationToken: '...a token...'
      });
    });
    it('will label authorizationToken as missing if field is null/undefined', () => {
      const event = {
        some: 'stuff',
        authorizationToken: null
      };
      expect(logHelper.removeAuthorizationToken(event)).to.deep.equal({
        some: 'stuff',
        authorizationToken: 'no token!'
      });
    });
    it('will do nothing if authorizationToken is not present at all', () => {
      const event = {
        some: 'stuff',
        more: 'stuff'
      };
      expect(logHelper.removeAuthorizationToken(event)).to.deep.equal(event);
    });
  });
});
