const v3 = require('../src/handlers/authorizerV3').authorizerV3;
const lambda = require('../index');
const getApiOptions = require('../src/utils/getApiOptions').getApiOptions;
const helper = require('./test_helper');
// const nock = require('nock');
const sinon = require('sinon');
const jwksClient = require('../src/utils/jwksClient');

const anOldButRealToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlJUQXdSRGMxTmpoRVFrSkJOamxETlRJM016TkZOamxFUlRRME5UbEVOalpEUXpORU1FSkdOZyJ9.eyJodHRwczovL2FjY29yZG8uaW8vYXBwX21ldGFkYXRhIjp7Im9yZ2FuaXphdGlvbnMiOlt7Im9yZ0lkIjoiaXBfOThmMzdiYTQtYmFmNC00ZTFmLWE5YWItYzg2YWIyMmIxZWNlIiwic3RhdHVzIjoiYWN0aXZlIiwicm9sZSI6Im93bmVyIiwic3VydmV5cyI6W10sImNhbXBhaWduU291cmNlIjoiVEMgaXBpZm55IiwiY2FtcGFpZ25UeXBlSWQiOjQ3LCJjYW1wYWlnbklkIjoxMDAwNn1dfSwiaXNzIjoiaHR0cHM6Ly9kZXYtaXBpZm55LmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1YjI5NmY3NWRmMzhmZDdhMTJjNzI3MTEiLCJhdWQiOlsiaHR0cHM6Ly9kZXYtYXBpLmFjY29yZG8uaW8iLCJodHRwczovL2Rldi1pcGlmbnkuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyOTUyNjg4NCwiZXhwIjoxNTI5NjEzMjg0LCJhenAiOiJuWXRROTVKQmkzUE5yd094cmJtczY5NlJYM1pmYXFHcSIsInNjb3BlIjoib3BlbmlkIHByb2ZpbGUifQ.iEdIfeQ2w7929WP_c8ogEX37zmMv-C185MHpr3gqk3QrSJWcxyrnc3ofN97FVWEdHzl50cFo7Z2YjA_5vnEqwzdyLEpRLu9AGiM09qTZitOAbSwA1o_vWCBFf-3Vpg1ojnRNXrrxWQogrP-9gfGSZcBcKMqeiIH08-bNUgmIkhA2K8_6UX1URfsrCNfoUW31dWGNyBcoNukI3pTqaR7cQvTZ5i0w8DQG-f5Pl6o5O7oYr0EcjWeGClt643apkeIjhJ7TKq_zrJ_Y66avEOPSdGtfETZ2Z0uwV3Zn9Wmy9-zY3zFBiAreYCP2li9f0Bp3-qQpBedIEfYG60JR8xaPsA';

const anAPiConfig = {
  "dev": {
    "allowedClients": [
      {
        "secret": "AQECAHh+WbHYdrY8IgMgDN+aLK1INXLpDCI7CLZ2ujU3XLecgQAAAGgwZgYJKoZIhvcNAQcGoFkwVwIBADBSBgkqhkiG9w0BBwEwHgYJYIZIAWUDBAEuMBEEDKf98OoHK/EVf78AywIBEIAl4WGYTGIvhOpLDoUDkM3jjGPSARqHySKAC9xgeeX9oTWZo7YWog==",
        "verifyOptions": {
          "issuer": "https://dev-accordo.au.auth0.com/",
          "audience": "BocVApIjvf84jewFfWVFH3zseVtZout2"
        },
        "encodeSecret": false
      },
      {
        "secret": "AQECAHh+WbHYdrY8IgMgDN+aLK1INXLpDCI7CLZ2ujU3XLecgQAAAGgwZgYJKoZIhvcNAQcGoFkwVwIBADBSBgkqhkiG9w0BBwEwHgYJYIZIAWUDBAEuMBEEDKf98OoHK/EVf78AywIBEIAl4WGYTGIvhOpLDoUDkM3jjGPSARqHySKAC9xgeeX9oTWZo7YWog==",
        "verifyOptions": {
          "issuer": "https://dev-ipifny.auth0.com/",
          "audience": "xKIAPF37AgONtCwbzNPjvaefslSBx1ar"
        },
        "encodeSecret": true
      },
      {
        "secret": "AQECAHh+WbHYdrY8IgMgDN+aLK1INXLpDCI7CLZ2ujU3XLecgQAAAGgwZgYJKoZIhvcNAQcGoFkwVwIBADBSBgkqhkiG9w0BBwEwHgYJYIZIAWUDBAEuMBEEDKf98OoHK/EVf78AywIBEIAl4WGYTGIvhOpLDoUDkM3jjGPSARqHySKAC9xgeeX9oTWZo7YWog==",
        "verifyOptions": {
          "issuer": "https://dev-accordo.au.auth0.com/",
          "audience": "UoSXztX9HwfDZp9xnUyDdzObdMOCgOjg"
        },
        "encodeSecret": true
      },
      {
        "secret": "n/a (JWKS)",
        "verifyOptions": {
          "issuer": "https://dev-ipifny.auth0.com/",
          "audience": 'https://dev-api.accordo.io'
        }
      }
    ]
  },
  "policies": [
    {

      "role": "ciUser",
      "resources": [
        {
          "verb": "GET",
          "resource": "/map/{sub}/*"
        },
        {
          "verb": "PUT",
          "resource": "/map/{sub}/*"
        },
        {
          "verb": "DELETE",
          "resource": "/map/{sub}/*"
        },
        {
          "verb": "GET",
          "resource": "/map/auth/{sub}"
        },
        {
          "verb": "GET",
          "resource": "/lansweeper/auth"
        },
        {
          "verb": "GET",
          "resource": "/mapbak/auth"
        },
        {
          "verb": "GET",
          "resource": "/map/auth"
        }
      ]
    }
  ],
  "version": "2"
}

const denyAllPolicy = {
  "policyDocument": {
    "Statement": [
      {
        "Action": "execute-api:Invoke",
        "Effect": "Deny",
        "Resource": [
          "arn:aws:execute-api:us-west-2:ouraccountid:theapiid(notname)/dev/*/*"
        ],
      }
    ],
    "Version": "2012-10-17",
  },
  "principalId": "auth0|5b300f037ddc051b6f8eef4d"
}
const apiInfo = getApiOptions('arn:aws:execute-api:us-west-2:ouraccountid:theapiid(notname)/dev/GET/somepath/withstuff')


  const app_metadata = {
    'https://accordo.io/app_metadata': {
    organizations: [{
      campaignId: 10006,
      campaignSource: "TC ipifny",
      campaignTypeId: 47,
      orgId: "ip_98f37ba4-baf4-4e1f-a9ab-c86ab22b1ece",
      role: "owner",
      status: "active",
      surveys: []
    }
  ]
}
}

let newToken;
describe('INTEGRATION lambda-handler', () => {
  describe('authorizerV3', () => {
    before(async () => {
      global.CI_MODE = true;
      newToken = await helper.getAuth0Token();
      process.env.dev_JWKS_URI = 'http://dev-ipifny.auth0.com/.well-known/jwks.json';
    });
    after(() => {
      process.env.dev_JWKS_URI = null;
    });

    it('it should generate a policy for good token', async () => {
      const payload = {
        event: {
          authorizationToken: newToken
        },
        apiInfo: apiInfo,
        options: anAPiConfig
      };
      const result = await v3(payload);
      expect(result).to.deep.equal(denyAllPolicy)
    });
  });
  describe('full run through lambda', () => {
    let aToken;
    before(async () => {
      aToken = await helper.getAuth0Token();
      process.env['prefix'] = 'testing-authorizer';
      process.env.dev_JWKS_URI = 'https://dev-ipifny.auth0.com/.well-known/jwks.json';
      global.CI_MODE = true;
    } );
    after(() => {
      process.env['prefix'] = null;
      process.env.dev_JWKS_URI = null;
      global.CI_MODE = false;
    } )
    it('should make a policy for a valid token', async () => {
      const event = {
      "type": "TOKEN",
      "authorizationToken": `bearer ${aToken}`,
      "methodArn": "arn:aws:execute-api:test-region:test-account:m9afc1r2oe/dev/GET/mapbak/auth"
      };

      const policy = await lambda.handler(event);
      expect(policy.policyDocument.Statement).to.deep.equal([
        {
          "Action": "execute-api:Invoke",
          "Effect": "Deny",
          "Resource": [
            "arn:aws:execute-api:test-region:test-account:m9afc1r2oe/dev/*/*"
          ]
        }
      ]);
    });
  })
});
