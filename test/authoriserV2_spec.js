const R = require('ramda');
const lambda = require('../index');
const createToken = require('./test_helper').createToken;
const testData = require('./test_data');

const deniedPolicy = testData.deniedPolicy;
const sampleEvent = testData.sampleEvent;

describe('INTEGRATION lambda-handler', () => {
  describe('authorizerV2', () => {
    it('should be able to use the encrypted envs to issue policy', async () => {
      const token = createToken(['superman']);
      const event = {
        type: "TOKEN",
        authorizationToken: token,
        methodArn: "arn:aws:execute-api:test-region:test-account:random-application/dev/POST/somethingNotMatter"
      };
      
      const policy = await lambda.handler(event);
      expect(policy).to.deep.equal({
        "policyDocument": {
          "Statement": [
            {
              "Action": "execute-api:Invoke",
              "Effect": "Deny",
              "Resource": [
                "arn:aws:execute-api:test-region:test-account:random-application/dev/*/*"
              ]
            }
          ],
          "Version": "2012-10-17"
        },
        "principalId": "mruser|ah5f8db"
      });
    });
    it('returns denyAll policy for invalid roles', async () => {
      const methodArn = "arn:aws:execute-api:eu-west-9:acmeco:random-application/dev/put/people";      
      //Push up the config to s3.
      //Config in place, lets test
      const event = R.clone(sampleEvent);
      event.authorizationToken = createToken(['superman']);
      event.methodArn = methodArn;
      const policy = await lambda.handler(event)
      expect(policy).to.deep.equal(deniedPolicy);
    });
  });
});
