const R = require('ramda');
const path = require('path');
const fs = require('fs');
const { expect } = require('chai');

const testHelper = require('../testHelperAuthorizer');

let tokens = {};

const verifyScenario = (scenarioFileName, folderPath) => {
  const scenarioOutcomeFileName = scenarioFileName.replace('input.json', 'output.json');
  const inputPathFileName = `${folderPath}/${scenarioFileName}`;
  const outputPathFileName = `${folderPath}/${scenarioOutcomeFileName}`;
  try {
    if (!fs.existsSync(outputPathFileName)) {
      throw new Error(`output file not found for ${scenarioFileName}`);
    }

    it(`response should match with the file: ${scenarioOutcomeFileName}`, async () => {
      const token = getCorrectToken(scenarioFileName);
      const inputFile = require(inputPathFileName);
      const outputFile = require(outputPathFileName);

      inputFile.authorizationToken = token;

      const response = await testHelper.lambdaLocalExecuteEvent(inputFile);

      expect(response).to.deep.equal(outputFile);
    });
  } catch (error) {
    console.log(error.message);
  }
};

const processGateway = folderPath => {
  const isOutcomeFile = name => name.includes('output.json');
  const scenarioFiles = R.reject(isOutcomeFile, R.filter(name => name.includes('.json'), fs.readdirSync(folderPath)));

  R.forEach(scenarioFileName => {
    const isMatchServiceName = process.env.serviceName ? scenarioFileName.includes(process.env.serviceName) : true;
    if (isMatchServiceName) verifyScenario(scenarioFileName, folderPath);
  }, scenarioFiles);
};

describe('Component - Authorizer test scenarios', async () => {
  before(async () => {
    tokens = await testHelper.getTokens();
    process.env.prefix = 'testing-authorizer';
    process.env.dev_ACO_JWKS_URI = 'https://dev-aco.auth0.com/.well-known/jwks.json';
    process.env.uat_ACO_JWKS_URI = 'https://prod-aco.auth0.com/.well-known/jwks.json';
    process.env.prod_ACO_JWKS_URI = 'https://prod-aco.auth0.com/.well-known/jwks.json';
  });

  const rootPath = path.resolve(__dirname, '../resource');
  const gateways = fs.readdirSync(rootPath);
  R.forEach(gateway => {
    const folderPath = path.resolve(__dirname, `../resource/${gateway}`);
    processGateway(folderPath);
  }, gateways);
});

const getCorrectToken = scenarioFileName => {
  const envAndRoleRegex = testHelper.getTokenEnvAndRoleRegex();
  const envAndRole = envAndRoleRegex.exec(scenarioFileName)[1];
  const token = tokens[envAndRole];
  return token;
};
