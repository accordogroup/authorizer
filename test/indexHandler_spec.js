const lambda = require('../index');
const R = require('ramda');
const getApiOptions = require('../src/utils/getApiOptions').getApiOptions;
const createToken = require('./test_helper').createToken;


describe('INTEGRATION Lambda Action', () => {
  describe('Environment Variables', () => {
    afterEach(() => {
      process.env['prefix'] = 'accordo-authorizer'; 
    })
    it('should check stage name', () => {
      let error;
      try {
        getApiOptions("arn:aws:execute-api:ireland-west-1:acmeco:sample-application/sample-stage/get/someinfo");
      } catch (e) {
        error = e;
      }
      expect(error.message).to.be.equal('Only [dev, qa, uat, prod] are supported stages');
    });
    it('will look for configs based on prefix env', async () => {
      const token = createToken(['superman']);
      const event = {
        type: "TOKEN",
        authorizationToken: token,
        methodArn: "arn:aws:execute-api:test-region:test-account:nonexist-app/dev/POST/somethingNotMatter"
      };
      process.env['prefix'] = 'doesntexist';
      let err;
      try {
        await lambda.handler(event);
      } catch (error) {
        err = error;
      }
      expect(err.message).to.equal('Unauthorized')
    });
  });
  describe('Unauthorized', () => {
    it('should return Unauthorized if there is no methodArn provided', async () => {
      const event = {};
      let err;
      try {
        await lambda.handler(event, { });
      } catch (error) {
        err = error;
      }
      expect(err.message).to.equal('Unauthorized');
    });
    
    it('should return Unauthorized if there is no authorizationToken provided', async () => {
      const event = { methodArn: 'methodArn' };
      let err;
      try {
        await lambda.handler(event, { });
      } catch (error) {
        err = error;
      }
      expect(err.message).to.equal('Unauthorized');
    });
  });
});



