const sinon = require('sinon');
const local = require('../src/gateways/local');
const policy = require('../src/utils/policyConfig');
const s3 = require('../src/gateways/s3');

const sandbox = sinon.createSandbox();

const dummyPolicy = {
  prod: {
    allowedClients: [
      {
        secret: 'nosecretshere',
        verifyOptions: {
          issuer: 'https://assist-ms.auth0.com/',
          audience: 'thiswontvalidateifused'
        },
        encodeSecret: false
      }
    ]
  },
  policies: [{
    role: 'CATUsers',
    resources:[
      { verb: '*', resource: '*' }
    ]
  }],
  version: '2'
};

describe('UNIT - utils', () => {
  describe('policyConfig', () => {
    describe('getPolicyConfig', () => {
      afterEach(() => {
        sandbox.restore();
      });
      it('can return policy from local if it is present', async () => {
        sandbox.stub(local, 'useLocal').returns(true);
        const stub = sandbox.stub(local, 'getConfigByKey').resolves(dummyPolicy);
        const result = await policy.getPolicyConfig({restApiId: '1234fake', stage: 'news'});
        expect(result).to.deep.equal(dummyPolicy);
        expect(stub.calledOnce).to.equal(true);
      });
      it('can return a policy from s3', async () => {
        sandbox.stub(local, 'useLocal').returns(false);
        const stub = sandbox.stub(s3, 'getConfigByKey').resolves(dummyPolicy);
        const result = await policy.getPolicyConfig({restApiId: '1234faked', stage: 'news'});
        expect(result).to.deep.equal(dummyPolicy);
        expect(stub.calledOnce).to.equal(true);
      });
      it('can return a policy from its own store', async () => {
        const result = await policy.getPolicyConfig({restApiId: '1234fake', stage: 'news'});
        expect(result).to.deep.equal(dummyPolicy);
      });
      it('will throw an error if no config is found', async () => {
        let error;
        try {
          await policy.getPolicyConfig({restApiId: '1234real', stage: 'news'});
        } catch (err) {
          error = err
        }
        expect(error.message).to.include('No potential configs found: ["1234real/config.json","1234real/news/config.json"]');
      });
    });
  });
});