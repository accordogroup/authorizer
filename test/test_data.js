const adminToken = {
  "app_metadata": {
    "signed_up": true,
    "roles": [
      "admin",
      "batman"
    ]
  },
  "iss": "https://accordo.auth0.com/",
  "sub": "windowslive|a2c99bad383fa0d2",
  "aud": "qyxQmeW8XgLnFuzxuJhBE36OHQJU7Y6K",
  "exp": 37459239249,
  "iat": 1459239249
};

const appConfig = {
  "verifyOptions": {
    "algorithm": 'HS256',
    "issuer": 'https://funtimes.acmeco.com',
    "audience": 'theAccountsDepartment'
  },
  policies: [
    {
      "role": "admin",
      "resources": [
        {"verb": "*", "resource": "*"}
      ]
    },
    {
      "role": "consultant",
      "resources": [
        {"verb": "GET", "resource": "/direct/*"}
      ]
    },
    {
      "role": "batman",
      "resources": [
        {"verb": "GET", "resource": "/data/{sub}"},
        {"verb": "PUT", "resource": "/data/{sub}"},
        {"verb": "POST", "resource": "/data/{sub}"}
      ]

    }
  ]

};

const deniedPolicy = {
  "principalId": "mruser|ah5f8db",
  "policyDocument": {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "execute-api:Invoke",
        "Effect": "Deny",
        "Resource": [
          "arn:aws:execute-api:eu-west-9:acmeco:random-application/dev/*/*"
        ]
      }
    ]
  }
};

const sampleEvent =
{
  "type": "TOKEN",
  "authorizationToken": "<Incoming bearer token>",
  "methodArn": "arn:aws:execute-api:<Region id>:<Account id>:sample-application/dev/<Method>/<Resource path>"
};


const expectedAdminPolicy = {
  "principalId": "windowslive|a2c99bad383fa0d2",
  "policyDocument": {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "execute-api:Invoke",
        "Effect": "Allow",
        "Resource": [
          "arn:aws:execute-api:ireland-west-1:acmeco:sample-application/dev/*/*"
        ]
      }
    ]
  }
};



const expectedConsultantPolicy = {
  "principalId": "windowslive|a2c99bad383fa0d2",
  "policyDocument": {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "execute-api:Invoke",
        "Effect": "Allow",
        "Resource": [
          "arn:aws:execute-api:ireland-west-1:acmeco:sample-application/dev/GET/direct/*"
        ]
      }
    ]
  }
};

const expectedBatmanPolicy = {
  "principalId": "windowslive|a2c99bad383fa0d2",
  "policyDocument": {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "execute-api:Invoke",
        "Effect": "Allow",
        "Resource": [
          "arn:aws:execute-api:ireland-west-1:acmeco:sample-application/dev/GET/data/a2c99bad383fa0d2",
          "arn:aws:execute-api:ireland-west-1:acmeco:sample-application/dev/PUT/data/a2c99bad383fa0d2",
          "arn:aws:execute-api:ireland-west-1:acmeco:sample-application/dev/POST/data/a2c99bad383fa0d2"
        ]
      }
    ]
  }
};


module.exports = {
  adminToken,
  appConfig,
  deniedPolicy,
  sampleEvent,
  expectedAdminPolicy,
  expectedConsultantPolicy,
  expectedBatmanPolicy
};
