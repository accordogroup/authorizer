const sinon = require('sinon');
const R = require('ramda');
const lambda = require('../index');
const policyConfig = require('../src/utils/policyConfig');
const samplePolicy = require('./testConfigv1.json');
const testData = require('./test_data');
const createToken = require('./test_helper').createToken;

const token = createToken({}, {
  "roles": [ "user" ],
  "temp": "false",
  "authorization": {
    "groups": [ "Experimental" ],
    "roles": [ "BetaFeatures" ],
    "permissions": [ "ShowChangePassword" ]
  }, 
  "organizations": [ { "orgId": "123", "role": "user" } ]
});
const sampleEvent = testData.sampleEvent;
const deniedPolicy = testData.deniedPolicy;
const sandbox = sinon.createSandbox();

describe('INTEGRATION lambda-handler', () => {
  describe('authoriserV1', () => {
    afterEach(() => {
      sandbox.restore();
    });
    it('should be able to use the encrypted envs to issue policy', async () => {
      const event = {
        type: "TOKEN",
        authorizationToken: token,
        methodArn: "arn:aws:execute-api:test-region:test-account:random-application/uat/POST/somethingNotMatter"
      };

      sandbox.stub(policyConfig, 'getPolicyConfig').resolves(samplePolicy);
      const policy = await lambda.handler(event);
      expect(policy.policyDocument).to.deep.equal({
        "Statement": [
          {
            "Action": "execute-api:Invoke",
            "Effect": "Allow",
            "Resource": [
              "arn:aws:execute-api:test-region:test-account:random-application/uat/GET/123"
            ]
          }
        ],
        "Version": "2012-10-17"
      })
    });
    it('returns denyAll policy for invalid roles', async () => {
      sandbox.stub(policyConfig, 'getPolicyConfig').resolves(samplePolicy);
      const methodArn = "arn:aws:execute-api:eu-west-9:acmeco:random-application/dev/put/people";
      const event = R.clone(sampleEvent);
      event.authorizationToken = createToken(['superman']);
      event.methodArn = methodArn;
      const policy = await lambda.handler(event)
      expect(policy).to.deep.equal(deniedPolicy);
    });
  })
});
