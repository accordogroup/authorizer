const R = require('ramda');
const sinon = require('sinon');
const organizations = require('../src/gateways/organizations');
const generatePolicy = require('../src/services/generatePolicy').generatePolicy;
const testData = require('./organizationPolicy_data');
const getApiOptions = require('../src/utils/getApiOptions').getApiOptions;

const sandbox = sinon.createSandbox();

const createTestPolicy = async (token, config) => {
  const apiOptions = getApiOptions('arn:aws:execute-api:us-west-2:003849295229:ky6vxel2ub/dev/GET');

  const policyArgs = {
    token: token || testData.adminToken,
    config: config || testData.appConfig
  };

  return generatePolicy(policyArgs.token, apiOptions, policyArgs.config);
};

describe('COMPONENT generatePolicy', () => {
  describe('organizationPolicy', () => {
    afterEach(() => {
      sandbox.restore();
    });
    it('should produce new org policies', async () => {
      const policy = await createTestPolicy();
      expect(policy.policyDocument.Statement).to.deep.equal([
        {
          'Action': 'execute-api:Invoke',
          'Effect': 'Allow',
          'Resource': [
            'arn:aws:execute-api:us-west-2:003849295229:ky6vxel2ub/dev/GET/20',
            'arn:aws:execute-api:us-west-2:003849295229:ky6vxel2ub/dev/PUT/20',
            'arn:aws:execute-api:us-west-2:003849295229:ky6vxel2ub/dev/GET/20/users',
            'arn:aws:execute-api:us-west-2:003849295229:ky6vxel2ub/dev/PUT/20/users',
            'arn:aws:execute-api:us-west-2:003849295229:ky6vxel2ub/dev/GET/30',
            'arn:aws:execute-api:us-west-2:003849295229:ky6vxel2ub/dev/GET/30/users'
          ]
        }
      ]);
    });

    it('should produce no org polices when user is not in an org', async () => {
      const emptyToken = R.merge(testData.adminToken, {
        'app_metadata': {
          'signed_up': true,
          'roles': [],
          'authorization': {
            'roles': [],
            'permissions': []
          },
          'organizations': []
        }
      });
      const policy = await createTestPolicy(emptyToken);
      expect(policy.policyDocument.Statement).to.deep.equal([
        {
          'Action': 'execute-api:Invoke',
          'Effect': 'Deny',
          'Resource': [
            'arn:aws:execute-api:us-west-2:003849295229:ky6vxel2ub/dev/*/*',
          ]
        }
      ]);
    });

    it('should ignore admin roles that dont have matching policies', async () => {
      const emptyToken = R.merge(testData.adminToken, {
        'app_metadata': {
          'signed_up': true,
          'roles': [],
          'authorization': {
            'roles': [],
            'permissions': []
          },
          'organizations': [
            { 'orgId': 10, 'role': 'admin' }
          ]
        }
      });
      const policy = await createTestPolicy(emptyToken);
      expect(policy.policyDocument.Statement).to.deep.equal([
        {
          'Action': 'execute-api:Invoke',
          'Effect': 'Deny',
          'Resource': [
            'arn:aws:execute-api:us-west-2:003849295229:ky6vxel2ub/dev/*/*'
          ]
        }
      ]);
    });

    it('should allow all access if the user contains an admin role', async () => {
      const emptyToken = R.merge(testData.adminToken, {
        'app_metadata': {
          'signed_up': true,
          'roles': ['admin'],
          'authorization': {
            'roles': [],
            'permissions': []
          },
          'organizations': [
            { 'orgId': 10, 'role': 'owner' }
          ]
        }
      });
      const policy = await createTestPolicy(emptyToken);
      expect(policy.policyDocument.Statement).to.deep.equal([
        {
          'Action': 'execute-api:Invoke',
          'Effect': 'Allow',
          'Resource': [
            'arn:aws:execute-api:us-west-2:003849295229:ky6vxel2ub/dev/*/*',
            'arn:aws:execute-api:us-west-2:003849295229:ky6vxel2ub/dev/GET/10',
            'arn:aws:execute-api:us-west-2:003849295229:ky6vxel2ub/dev/PUT/10',
            'arn:aws:execute-api:us-west-2:003849295229:ky6vxel2ub/dev/GET/10/users',
            'arn:aws:execute-api:us-west-2:003849295229:ky6vxel2ub/dev/PUT/10/users'
          ]
        }
      ]);
    });
  });
});

