const adminToken = {
  'app_metadata': {
    'signed_up': true,
    'roles': [],
    'authorization': {
      'roles': [],
      'permissions': []
    },
    'organizations': [
      { 'orgId': 10, 'role': 'admin' },
      { 'orgId': 20, 'role': 'owner' },
      { 'orgId': 30, 'role': 'user' }
    ]
  },
  'iss': 'https://accordo.auth0.com/',
  'sub': 'windowslive|a2c99bad383fa0d2',
  'aud': 'qyxQmeW8XgLnFuzxuJhBE36OHQJU7Y6K',
  'exp': 37459239249,
  'iat': 1459239249
};

const appConfig = {
  'version': '2',
  'stage': 'dev',
  'dev': {
    "allowedClients": [
      {
        "secret": "AQECAHh+WbHYdrY8IgMgDN+aLK1INXLpDCI7CLZ2ujU3XLecgQAAAKIwgZ8GCSqGSIb3DQEHBqCBkTCBjgIBADCBiAYJKoZIhvcNAQcBMB4GCWCGSAFlAwQBLjARBAylzJxBsqCmFexMGOUCARCAW5Pl4t3gz17JKUptgSWHTMDBA5PRIqoXSWcg89c/WSpsFAvJvzks8NKcXYLlVr9vXIH4KxVPA6ncOjRDECIxhPLF3LGXc2zqBtRF/LaRtdlKbSs/JmjnpKtK5TE=",
        "verifyOptions": {
          "audience": "I6LFWN2H6Dq2utwvVrdBfSp6sOnRpnaD",
          "issuer": "https://dev-accordo.au.auth0.com/"
        }
      },
      {
        "secret": "AQECAHh+WbHYdrY8IgMgDN+aLK1INXLpDCI7CLZ2ujU3XLecgQAAAKIwgZ8GCSqGSIb3DQEHBqCBkTCBjgIBADCBiAYJKoZIhvcNAQcBMB4GCWCGSAFlAwQBLjARBAyJqCq0hbR5dl+ImF4CARCAW06AVzTcUdtkO0g45zGtRTViR8HIZoYxeA904DCggun+qVk0hjqpW96/gMJTUIn8lnToOI6egqwZlj9y5PW7pwrvFAmx49F6T2TLQk5HO+kbX4ZB1c6htDqUA8Y=",
        "verifyOptions": {
          "audience": "BocVApIjvf84jewFfWVFH3zseVtZout2",
          "issuer": "https://dev-accordo.au.auth0.com/"
        }
      }
    ]
  },
  'policies': [
    {
      'role': 'admin',
      'resources': [
        {
          'verb': '*',
          'resource': '*'
        }
      ]
    },
    {
      'role': 'partner',
      'resources': [{
        'verb': 'GET',
        'resource': '/assessments/{sub}/organisations/*'
      }]
    },
    {
      'role': 'consultant',
      'resources': [
        {
          'verb': 'GET',
          'resource': '/direct/*'
        },
        {
          'verb': 'GET',
          'resource': '/assessments/{sub}/organisations/*'
        }
      ]
    },
    {
      'orgRole': 'partner',
      'resources': [
        {
          'verb': 'GET',
          'resource': '/{orgId}'
        }
      ]
    },
    {
      'orgRole': 'owner',
      'resources': [
        {
          'verb': 'GET',
          'resource': '/{orgId}'
        },
        {
          'verb': 'PUT',
          'resource': '/{orgId}'
        },
        {
          'verb': 'GET',
          'resource': '/{orgId}/users'
        },
        {
          'verb': 'PUT',
          'resource': '/{orgId}/users'
        }
      ]
    },
    {
      'orgRole': 'user',
      'resources': [
        {
          'verb': 'GET',
          'resource': '/{orgId}'
        },
        {
          'verb': 'GET',
          'resource': '/{orgId}/users'
        }
      ]
    }
  ]
};

const expectedPositiveOuput = {
  'principalId': 'windowslive|a2c99bad383fa0d2',
  'policyDocument': {
    'Statement': [
      {
        'Action': 'execute-api:Invoke',
        'Effect': 'Allow',
        'Resource': [
          'arn:aws:execute-api:us-west-2:003849295229:462u7v9yii/dev/GET/20',
          'arn:aws:execute-api:us-west-2:003849295229:462u7v9yii/dev/PUT/20',
          'arn:aws:execute-api:us-west-2:003849295229:462u7v9yii/dev/GET/30'
        ]
      }
    ],
    'Version': '2012-10-17'
  }
};

module.exports = { adminToken, expectedPositiveOuput, appConfig };
