const local = require('../src/gateways/local');

describe('UNIT - gateways', () => {
  describe('local', () => {
    describe('getConfigByKey', () => {
      it('will return configs that exist', async () => {
        const result = await local.getConfigByKey('0eiow0olh4/config.json');
        expect(result).to.have.keys(['prod', 'policies', 'version']);
      });
      it('will return undefined with configs that dont exist', async () => {
        const result = await local.getConfigByKey('noexisty/config.json');
        expect(result).to.equal(undefined);
      });
      it('will throw an error if it encounters an unexpected one', async () => {
        let error;
        try {
          await local.getConfigByKey('0eiow0olh4');
        } catch (err) {
          error = err;
        }
        expect(error).to.be.ok;
      });
    });
  });
});