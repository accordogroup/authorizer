const ENCRYPTED_ENVS = require('../src/constants').ENCRYPTED_ENVS;
const kmsHelper = require('../src/utils/kms_helper');

describe('INTEGRATION kms_helper', () => {
  describe('normalize', () => {
    it('can normalize encrpyted envs', async () => {
      envs = {
        dev_secret: 'AQECAHh+WbHYdrY8IgMgDN+aLK1INXLpDCI7CLZ2ujU3XLecgQAAAKIwgZ8GCSqGSIb3DQEHBqCBkTCBjgIBADCBiAYJKoZIhvcNAQcBMB4GCWCGSAFlAwQBLjARBAwfNXuUv2Cq+2upGhECARCAW2WEzXkwbwNGp82pNk+NcF/AGSd4l/HgMA+1eo3mawbX5l4X+ens3lU+aICOPNWaj8RMZgDxxuSPaWx9R1bpwAWbh/OwSi05H4AcHWp4xZceFGTNck7Vc3EKC5w='
      }
      decryptEvns = await kmsHelper.normalize(ENCRYPTED_ENVS, envs);
      expect(decryptEvns.dev_secret).to.be.not.equal(envs.dev_secret);
    });
  })
  describe('decrypt', () => {
    it('can decrypt a given key in a value', async () => {
      const value = {
        notsecret: 'thisshouldbeunaffected',
        secret: 'AQECAHh+WbHYdrY8IgMgDN+aLK1INXLpDCI7CLZ2ujU3XLecgQAAAGkwZwYJKoZIhvcNAQcGoFowWAIBADBTBgkqhkiG9w0BBwEwHgYJYIZIAWUDBAEuMBEEDBvYM0ED6SA757zclgIBEIAmYtVYD24Zze3pmxsI91q7jz9yjD/BbA4E/Wx9hdQuyyJpAbdMXXo='
      }
      const result = await kmsHelper.decrypt(value, 'secret');
      expect(result).to.deep.equal({ notsecret: 'thisshouldbeunaffected', secret: 'password123' });
    });
    it('will error if key doesnt exist', async () => {
      const value = {
        notsecret: 'thisshouldbeunaffected'
      }
      let error;
      try {
        await kmsHelper.decrypt(value, 'secret');
      } catch (err) {
        error = err;
      }
      expect(error).to.be.ok;
    });
    it('will error if key cant be decrypted', async () => {
      const value = {
        notsecret: 'thisshouldbeunaffected',
        secret: 'AQECAHh+WbHYdrY8IgMgDN+aLK1INXLpDCI7CLZ2ujU3XLecgQAAAGkwZwYJKoZIhvcNAQcGoFoADBTBgkqhkiG9w0BBwEwHgYJYIZIAWUDBAEuMBEEDBvYM0ED6SA757zclgIBEIAmYtVYD24Zze3pmxsI91q7jz9yjD/BbA4E/Wx9hdQuyyJpAbdMXXo='
      }
      let error;
      try {
        await kmsHelper.decrypt(value, 'secret');
      } catch (err) {
        error = err;
      }
      expect(error).to.be.ok;
    });
  })
});
