const process = require('process');
const path = require('path');
const s3 = require('@auth0/s3');

const bucketName = process.argv[2];
const region = process.argv[3];
const authorizer = `${process.argv[4] || 'testing'}-authorizer`;
const sourceDir = path.resolve(__dirname, './policies/accordo.config/authorizer');

console.log(`uploading to ${bucketName}/${authorizer}`);
console.log(`from ${sourceDir}`);

const client = s3.createClient({
  s3RetryCount: 3,
  s3Options: {
    region
  }
});

// modified from s3 readme
const getS3Params = (localFile, stat, callback) => {
  const err = null;
  const s3Params = (localFile.includes('.json'))
    ? { ContentType: 'application/octet-stream' }
    : null;
  // pass `null` for `s3Params` if you want to skip uploading this file
  callback(err, s3Params);
};

// deleteRemoved will wipe contents of bucket from prefix 'folder' level
// so if uploaded to testing, everything in testing will be cleared, but prod would be ok
const uploadFolder = (client, sourceDir, bucketName, prefix) => {
  const params = {
    localDir: sourceDir,
    deleteRemoved: true,
    s3Params: {
      Bucket: bucketName,
      Prefix: prefix
    },
    getS3Params: getS3Params
  };

  var uploader = client.uploadDir(params);
  uploader.on('error', function (err) {
    console.error('unable to sync:', err.stack);
    process.exit(1);
  });
  uploader.on('progress', function () {
    console.log('progress', uploader.progressAmount, uploader.progressTotal);
  });
  uploader.on('end', function () {
    console.log('done uploading directory');
  });
};

uploadFolder(client, sourceDir, bucketName, authorizer);
