### Get Started

https://accordo.atlassian.net/wiki/spaces/EN/pages/70177210/How+to+play+with+API+authorizer

We have two authorizer lambda functions, testing-authorizer for functional tests and experimenting and accordo-authorizer for Dev, Uat and Prod api gateways

- accordo-authorizer
- testing-authorizer

The policy configs are deployed to s3 for both the testing-authorizer and accordo-authorizer under accordo.config bucket. Local tests will get them from the local copy.

## S3 Bucket to API Mapping

NB: Shows only those APIs which have an authorizer

| API                           | S3 Bucket Name                                     | Clients in use                                                                                                                                                                                                   |
| ----------------------------- | -------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Admin API                     | accordo.config/{environment}-authorizer/b74fyp73ql | dev-admin.accordo.io,uat-admin.accordo.io,admin.accordo.io                                                                                                                                                       |
| Provisioning API              | accordo.config/{environment}-authorizer/t03qvlskra | dev-webapp.accordo.com,uat-webapp.accordo.com,app.accordo.com                                                                                                                                                    |
| Organization API  (DEV)       | accordo.config/{environment}-authorizer/zimgfbm1m9 | dev-optimizer.accordo.com|
| Organization API  (UAT)       | accordo.config/{environment}-authorizer/4lyozxj8mf | uat-optimizer.accordo.com|
| Organization API  (PROD)      | accordo.config/{environment}-authorizer/7vu6pg64t1 | optimizer.accordo.com|
| ORG import-export API         | accordo.config/{environment}-authorizer/7o245kh65c | dev-ipifny, uat-ipifny, prod-ipifny                                                                                                                                                                              |
| Search API                    | accordo.config/{environment}-authorizer/mj16k2feqg | dev-webapp.accordo.com,uat-webapp.accordo.com,app.accordo.com                                                                                                                                                    |
| CSA Product                   | accordo.config/{environment}-authorizer/6o8mwkbt0f | dev-compliance.assist.ms,dev-ipifny,dev-lbc,uat-compliance.assist.ms,uat-ipifny,uat-lbc,compliance.assist.ms,prod-ipifny,prod-lbc                                                                                |
| compliance                    | accordo.config/{environment}-authorizer/phydkt2c4l | dev-compliance.assist.ms,dev-ipifny,dev-lbc,uat-compliance.assist.ms,uat-ipifny,uat-lbc,compliance.assist.ms,prod-ipifny,prod-lbc, Healthcheck                                                                   |
| Discovery                     | accordo.config/{environment}-authorizer/m9afc1r2oe | dev-compliance.assist.ms,dev-ipifny,dev-lbc,uat-compliance.assist.ms,uat-ipifny,uat-lbc,compliance.assist.ms,prod-ipifny,prod-lbc                                                                                |
| assessment-api                | accordo.config/{environment}-authorizer/462u7v9yii | dev-compliance.assist.ms,dev-ipifny,dev-lbc,uat-compliance.assist.ms,uat-ipifny,uat-lbc,compliance.assist.ms,prod-ipifny,prod-lbc                                                                                |
| Notification API              | accordo.config/{environment}-authorizer/n71d4nw0hb | dev-compliance.assist.ms,dev-ipifny,dev-lbc,uat-compliance.assist.ms,uat-ipifny,uat-lbc,compliance.assist.ms,prod-ipifny,prod-lbc                                                                                |
| Survey API                    | accordo.config/{environment}-authorizer/87szgftlw5 | dev-compliance.assist.ms,dev-ipifny,dev-lbc,uat-compliance.assist.ms,uat-ipifny,uat-lbc,compliance.assist.ms,prod-ipifny,prod-lbc                                                                                |
| ADMIN                         | accordo.config/{environment}-authorizer/s42r3d9hkc | Accordo-Admin                                                                                                                                                                                                    |
| validation                    | accordo.config/{environment}-authorizer/ruivbad4t7 | dev-webapp.accordo.com,uat-webapp.accordo.com,app.accordo.com                                                                                                                                                    |
| Account API                   | accordo.config/{environment}-authorizer/9fj8dqclsf | dev-compliance.assist.ms,dev-ipifny,dev-lbc,uat-compliance.assist.ms,uat-ipifny,uat-lbc,compliance.assist.ms,prod-ipifny,prod-lbc                                                                                |
| Transaction API (DEV)         | accordo.config/{environment}-authorizer/mnb23ub7r9 | dev-compliance.assist.ms,dev-ipifny,dev-lbc                                                                                                                                                                      |
| Transaction API (UAT)         | accordo.config/{environment}-authorizer/j2ndynk4pf | uat-compliance.assist.ms,uat-ipifny,uat-lbc                                                                                                                                                                      |
| Transaction API (PROD)        | accordo.config/{environment}-authorizer/qp6ecpltv1 | prod-compliance.assist.ms,prod-ipifny,prod-lbc                                                                                                                                                                   |
| Ipifny CRM                    | accordo.config/{environment}-authorizer/lp6ob4owrh | ipifny-crm-web-api                                                                                                                                                                                               |
| Testing                       | accordo.config/{environment}-authorizer/5f9ofsw0mh | currantly no config file. Added as placeholder                                                                                                                                                                   |
| Deployment API                | accordo.config/{environment}-authorizer/49dbccxro3 | dev-ipifny, uat-ipifny, prod-ipifny                                                                                                                                                                              |
| Position Questions API        | accordo.config/{environment}-authorizer/5zrh76zxpl | dev-ipifny, uat-ipifny, prod-ipifny, Healthcheck                                                                                                                                                                 |
| File Manager                  | accordo.config/{environment}-authorizer/9c41ifyx98 | dev-ipifny, uat-ipifny, prod-ipifny, Healthcheck                                                                                                                                                                 |
| Campaigns API                 | accordo.config/{environment}-authorizer/f5ayd6uez7 | dev-ipifny, uat-ipifny, prod-ipifny                                                                                                                                                                              |
| Reporting Services (DEV)      | accordo.config/{environment}-authorizer/nrk75jw8oa | dev-insight.assist.ms                                                                                                                                                                                            |
| Reporting Services (UAT)      | accordo.config/{environment}-authorizer/c6e8yge1o5 | uat-insight.assist.ms                                                                                                                                                                                            |
| Reporting Services (PROD)     | accordo.config/{environment}-authorizer/0eiow0olh4 | insight.assist.ms                                                                                                                                                                                                |
| Consumption Service (DEV)     | accordo.config/{environment}-authorizer/m55wl6ghdb | dev-ipifny, Healthcheck, dev-webapp.accordo.com, dev-aco                                                                                                                                                         |
| Consumption Service (UAT)     | accordo.config/{environment}-authorizer/m0en88k0u6 | uat-ipifny, Healthcheck, uat-webapp.accordo.com, prod-aco                                                                                                                                                        |
| Consumption Service (PROD)    | accordo.config/{environment}-authorizer/pa3z4au98d | prod-ipifny, Healthcheck, app.accordo.com, prod-aco                                                                                                                                                              |
| Healthcheck Service (DEV)     | accordo.config/{environment}-authorizer/gawu8c7028 | Healthcheck                                                                                                                                                                                                      |
| Healthcheck Service (UAT)     | accordo.config/{environment}-authorizer/j3wg88aw98 | Healthcheck                                                                                                                                                                                                      |
| Healthcheck Service (PROD)    | accordo.config/{environment}-authorizer/1oydcjj9ng | Healthcheck                                                                                                                                                                                                      |
| Assessment Proxy API (DEV)    | accordo.config/{environment}-authorizer/2dbjka6c2c | dev-insight.assist.ms                                                                                                                                                                                            |
| Assessment Proxy API (UAT)    | accordo.config/{environment}-authorizer/eh7o4falsj | uat-insight.assist.ms                                                                                                                                                                                            |
| Assessment Proxy API (PROD)   | accordo.config/{environment}-authorizer/3l8bv0n4xj | insight.assist.ms                                                                                                                                                                                                |
| Azure Optimizer (DEV)         | accordo.config/{environment}-authorizer/lugx0wlbj3 | dev-ipifny, Healthcheck, dev-webapp.accordo.com                                                                                                                                                                  |
| Azure Optimizer (UAT)         | accordo.config/{environment}-authorizer/jtjdiooim8 | uat-ipifny, Healthcheck, uat-webapp.accordo.com                                                                                                                                                                  |
| Azure Optimizer (PROD)        | accordo.config/{environment}-authorizer/ei5diuizdi | prod-ipifny, Healthcheck, app.accordo.com                                                                                                                                                                        |
| Search Service (DEV)          | accordo.config/{environment}-authorizer/ixiendb3zc | dev-aco, Healthcheck                                                                                                                                                                                             |
| Search Service (UAT)          | accordo.config/{environment}-authorizer/jx6llp6bhk | prod-aco, Healthcheck                                                                                                                                                                                            |
| Search Service (PROD)         | accordo.config/{environment}-authorizer/s7bg4nhyzf | prod-aco, Healthcheck                                                                                                                                                                                            |
| Search Index Service (DEV)    | accordo.config/{environment}-authorizer/qpz5ljo8d5 | dev-aco, Healthcheck                                                                                                                                                                                             |
| Search Index Service (UAT)    | accordo.config/{environment}-authorizer/2nvy7ydvik | prod-aco, Healthcheck                                                                                                                                                                                            |
| Search Index Service (PROD)   | accordo.config/{environment}-authorizer/f8giaa7og6 | prod-aco, Healthcheck                                                                                                                                                                                            |
| Payment gateway (DEV)         | accordo.config/{environment}-authorizer/7z8vhddbi4 | dev-aco, Healthcheck                                                                                                                                                                                             |
| Payment gateway (UAT)         | accordo.config/{environment}-authorizer/3ve7c53fn0 | uat-aco, Healthcheck                                                                                                                                                                                             |
| Payment gateway (PROD)        | accordo.config/{environment}-authorizer/6vbje42ak6 | prod-aco, Healthcheck                                                                                                                                                                                            |
| Integrations Service (DEV)    | accordo.config/{environment}-authorizer/np8ue58ysl | dev-aco, Healthcheck                                                                                                                                                                                             |
| Integrations Service (UAT)    | accordo.config/{environment}-authorizer/zqf1tq3556 | prod-aco, Healthcheck                                                                                                                                                                                            |
| Integrations Service (PROD)   | accordo.config/{environment}-authorizer/gb4miu3kq8 | prod-aco, Healthcheck                                                                                                                                                                                            |
| Partner Center Service (DEV)  | accordo.config/{environment}-authorizer/4yl868jb9j | dev-aco, Healthcheck                                                                                                                                                                                             |
| Partner Center Service (UAT)  | accordo.config/{environment}-authorizer/zlc6pz0c38 | prod-aco, Healthcheck                                                                                                                                                                                            |
| Partner Center Service (PROD) | accordo.config/{environment}-authorizer/dmz2wxw0wh | prod-aco, Healthcheck                                                                                                                                                                                            |
| Notification Auth (DEV)       | accordo.config/{environment}-authorizer/f9704lan3k | dev-aco, Healthcheck                                                                                                                                                                                             |
| Notification Auth (UAT)       | accordo.config/{environment}-authorizer/pmjkiie8h9 | prod-aco, Healthcheck                                                                                                                                                                                            |
| Notification Auth (PROD)      | accordo.config/{environment}-authorizer/mj8y48681m | prod-aco, Healthcheck                                                                                                                                                                                            |

Note:
IoT API is only found on Dev stage and it doesn't have any config file under the S3 Accordo.config folder.
Will not be migrated as it is currently failing under the original authorizer.

#### Run Tests

##### Run all tests

```
  // authorizer test
  npm test
  // policy test
  npm run test:component
```

##### Run specify service tests

```
export serviceName=organization-api
npm run test:component
```

#### Version Two Policy

To utilize the features available in the version two policies your policy must contain a key value at the root level like the following:
'version': '2'.

Having this key/value will enable the policy to contain multiple environments configuration and multiple clients per environment e.g.

```
{
  'version': '2',
  'dev': {
      "allowedClients": [
        {
          "secret": "secret",
          "verifyOptions": {
            "audience": "anAudience",
            "issuer": "anIssuer"
          }
        },
        {
          "secret": "AQECAHh+WbHYdrY8IgMgDN+aLK1INXLpDCI7CLZ2ujU3XLecgQAAAKIwgZ8GCSqGSIb3DQEHBqCBkTCBjgIBADCBiAYJKoZIhvcNAQcBMB4GCWCGSAFlAwQBLjARBAyJqCq0hbR5dl+ImF4CARCAW06AVzTcUdtkO0g45zGtRTViR8HIZoYxeA904DCggun+qVk0hjqpW96/gMJTUIn8lnToOI6egqwZlj9y5PW7pwrvFAmx49F6T2TLQk5HO+kbX4ZB1c6htDqUA8Y=",
          "verifyOptions": {
            "audience": "BocVApIjvf84jewFfWVFH3zseVtZout2",
            "issuer": "https://dev-accordo.au.auth0.com/"
          }
        }
      ]
    }
  }
  'uat': {
      "allowedClients": [
        {
          "secret": "secret",
          "verifyOptions": {
            "audience": "anAudience",
            "issuer": "anIssuer"
          }
        }
      ]
    }
  }
}
```

#### Build

Build handled by bitbucket pipeline, when push commit to remote it trigger the default build in bitbucket-pipelines.yml

#### Deploy & Provision

Deploy handled by bitbucket master pipeline, check the build settings from bitbucket-pipelines.yml

There's a custom pipeline to deploy to testing-authorizer so you can hook changes up to a gateway before you merge to master
and another 2 custom pipelines to run the smoke tests (against testing or accordo authorizer)

To run a custom pipeline we have to manually trigger it

1. go to [bitbucket repository branches page](https://bitbucket.org/accordogroup/accordo-authorizer/branches/)
2. find the branch to run
3. click the '...' at end of the master branch row
4. select Run pipeline for a branch
5. in the pipeline dropdown select the one you want to run

## Testing the authorizer event in local

Make an input and expected output json in

```
./test/resource
// naming format:
<api>-<token name>-input.json
<api>-<token name>-output.json
/*
<api> is just a convenient name of the gateway auth being tested e.g. account-api
<token name> is one of:
  dev-analyst
  uat-analyst
  prod-analyst
  dev-consultant
  uat-consultant
  prod-consultant
  dev-orgowner
  uat-orgowner
  prod-orgowner
  dev-catusers
  uat-catusers
  prod-catusers
  dev-partner
  uat-partner
  prod-partner
  dev-padmin
  uat-padmin
  prod-padmin
  dev-pmember
  uat-pmember
  prod-pmember  
And corresponds to a long life token/user credentials stored in secret manager
*/
```

Run the component test and check results to see if yours passed (if you want to focus on just yours, move the others to a temp folder)

### Run component test for single service

```
export serviceName=integrations-service
npm run test:component
```

## Testing the authorizer event in CLI

Assuming you have AWS CLI installed and the necessary permissions then:

1. Make your input json file, preferably at the folder you are executing the command from. E.g.

```
{
  "type": "TOKEN",
  "authorizationToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhcHBfbWV0YWRhdGEiOnsicm9sZXMiOlsidXNlciJdLCJ0ZW1wIjoiZmFsc2UiLCJhdXRob3JpemF0aW9uIjp7Imdyb3VwcyI6WyJFeHBlcmltZW50YWwiXSwicm9sZXMiOlsiQmV0YUZlYXR1cmVzIl0sInBlcm1pc3Npb25zIjpbIlNob3dDaGFuZ2VQYXNzd29yZCJdfSwib3JnYW5pemF0aW9ucyI6W3sib3JnSWQiOiIxMjMiLCJyb2xlIjoib3duZXIifSx7Im9yZ0lkIjoiNmJkYmI2NDItMDAzMi00M2M1LWFmMTAtYjU1Zjg3MGY3M2QxIiwicm9sZSI6Im93bmVyIn1dfSwiaXNzIjoiaHR0cHM6Ly9kZXYtYWNjb3Jkby5hdS5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NTg1YWYyODIxYTlmYjY2NTdhZjQxNjBiIiwiYXVkIjoiQm9jVkFwSWp2Zjg0amV3RmZXVkZIM3pzZVZ0Wm91dDIiLCJleHAiOjE1MDU3ODY3ODMsImlhdCI6MTUwNTc2ODc4M30.5RW5ousfaN_rugcs9Br0_0-swJKQHEft9hX1PmA8glw",
  "methodArn": "arn:aws:execute-api:us-west-2:003849295229:462u7v9yii/dev/POST/"
}
```

Saved to input.json

Open the console of your choice at the location where you saved your input and put in the following command (good for windows + bash):

```
aws lambda invoke --function-name accordo-authorizer --region us-west-2 --payload file://input.json  outputfile.json
```

Change the name accordo-authorizer to testing-authorizer if you want to try on testing. There are some extra options that can be passed with this command described [here](https://docs.aws.amazon.com/lambda/latest/dg/with-userapp-walkthrough-custom-events-invoke.html) but they are not needed for the test.

## Testing the authorizer event in lambda function

This is quite risky for prod now that AWS have their blue box of automatic code reversion. USE CLI METHOD FOR PRODUCTION TESTS

1.  Go to Lambda Management Console and find the authorizer that needs to be tested (accordo-authorizer or testing-authorizer)

https://us-west-2.console.aws.amazon.com/lambda/home?region=us-west-2#/functions/

2. Using the request below, go to "Actions" and select "Configure test event", then "save and test"

example:

Example Request endpoint -> /dev/POST/:

```
{
  "type": "TOKEN",
  "authorizationToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhcHBfbWV0YWRhdGEiOnsicm9sZXMiOlsidXNlciJdLCJ0ZW1wIjoiZmFsc2UiLCJhdXRob3JpemF0aW9uIjp7Imdyb3VwcyI6WyJFeHBlcmltZW50YWwiXSwicm9sZXMiOlsiQmV0YUZlYXR1cmVzIl0sInBlcm1pc3Npb25zIjpbIlNob3dDaGFuZ2VQYXNzd29yZCJdfSwib3JnYW5pemF0aW9ucyI6W3sib3JnSWQiOiIxMjMiLCJyb2xlIjoib3duZXIifSx7Im9yZ0lkIjoiNmJkYmI2NDItMDAzMi00M2M1LWFmMTAtYjU1Zjg3MGY3M2QxIiwicm9sZSI6Im93bmVyIn1dfSwiaXNzIjoiaHR0cHM6Ly9kZXYtYWNjb3Jkby5hdS5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NTg1YWYyODIxYTlmYjY2NTdhZjQxNjBiIiwiYXVkIjoiQm9jVkFwSWp2Zjg0amV3RmZXVkZIM3pzZVZ0Wm91dDIiLCJleHAiOjE1MDU3ODY3ODMsImlhdCI6MTUwNTc2ODc4M30.5RW5ousfaN_rugcs9Br0_0-swJKQHEft9hX1PmA8glw",
  "methodArn": "arn:aws:execute-api:us-west-2:003849295229:462u7v9yii/dev/POST/"
}
```

3. Example Response:

```
{
  "principalId": "auth0|595c19cbf661667fe781a56d",
  "policyDocument": {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "execute-api:Invoke",
        "Effect": "Allow",
        "Resource": [
          "arn:aws:execute-api:us-west-2:003849295229:462u7v9yii/dev/GET/user/595c19cbf661667fe781a56d",
          "arn:aws:execute-api:us-west-2:003849295229:462u7v9yii/dev/PUT/user/595c19cbf661667fe781a56d/*",
          "arn:aws:execute-api:us-west-2:003849295229:462u7v9yii/dev/GET/products",
          "arn:aws:execute-api:us-west-2:003849295229:462u7v9yii/dev/GET/assessments/595c19cbf661667fe781a56d/organisations/*",
          "arn:aws:execute-api:us-west-2:003849295229:462u7v9yii/dev/PUT/assessments/595c19cbf661667fe781a56d/*",
          "arn:aws:execute-api:us-west-2:003849295229:462u7v9yii/dev/*/*"
        ]
      }
    ]
  }
}
```
