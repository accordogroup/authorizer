const R = require('ramda');
const authorizerV1 = require('./src/handlers/authorizerV1').authorizerV1;
const authorizerV2 = require('./src/handlers/authorizerV2').authorizerV2;
const authorizerV3 = require('./src/handlers/authorizerV3').authorizerV3;
const validator = require('./src/utils/event_validator');
const getApiOptions = require('./src/utils/getApiOptions').getApiOptions;
const policyConfig = require('./src/utils/policyConfig');
const logHelper = require('./src/utils/logHelper');
const token = require('./src/utils/validateToken');

const policyVersionTwo = payload => (
  R.equals('2', R.pathOr('na', ['options', 'version'], payload))
);

const isJWKSToken = payload => (token.eventTokenHasKid(payload.event));

/**
 * https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-lambda-authorizer-input.html
 * @param {object} event
 * {
 *  "type":"TOKEN",
 *  "authorizationToken":"<caller-supplied-token>",
 *  "methodArn":"arn:aws:execute-api:<regionId>:<accountId>:<apiId>/<stage>/<method>/<resourcePath>"
 * }
 * https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-lambda-authorizer-output.html
 * @returns {object} policy for token e.g. sample iam policy
 * {
 *  "principalId": "user",
 *  "policyDocument": {
 *    "Version": "2012-10-17",
 *     "Statement": [{
 *        "Action": "execute-api:Invoke",
 *        "Effect": "Deny",
 *        "Resource": "arn:aws:execute-api:us-west-2:123456789012:ymy8tbxw7b/*"
 *      }]
 *   }
 * }
 * or returns unauthorized error to trigger 401
 */
exports.handler = async function (event) {
  try {
    logHelper.log('handler_event', logHelper.removeAuthorizationToken(event));
    if (!validator.validate(event)) {
      const missingFields = validator.findMissingFields(event);
      throw new Error(`Not all required fields are provided. {${JSON.stringify(missingFields)}} are missing`);
    }
    const apiInfo = getApiOptions(event.methodArn);
    const options = await policyConfig.getPolicyConfig(apiInfo);
    const payload = {event, options, apiInfo};
    const policy = await R.cond([
      [isJWKSToken, authorizerV3],
      [policyVersionTwo, authorizerV2],
      [R.T, authorizerV1]
    ])(payload);
    return policy;
  } catch (error) {
    logHelper.log('handler_error', error);
    // only throw unauthorized to avoid leaking information about authentication
    // gateway will return 401 with no details if do this
    throw new Error('Unauthorized');
  }
};
