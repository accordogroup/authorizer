const R = require('ramda');
const SUPPORTED_STAGES = require('../constants').SUPPORTED_STAGES;

const getApiOptions = (arn) => {
  // parse the ARN from the incoming event
  const apiOptions = {};
  const tmp = arn.split(':');
  const apiGatewayArnTmp = tmp[5].split('/');
  apiOptions.awsAccountId = tmp[4];
  apiOptions.region = tmp[3];
  apiOptions.restApiId = apiGatewayArnTmp[0];
  apiOptions.stage = apiGatewayArnTmp[1];
  if (!R.contains(apiOptions.stage, SUPPORTED_STAGES)) {
    throw new Error(`Only [${R.join(', ')(SUPPORTED_STAGES)}] are supported stages`);
  }

  apiOptions.method = apiGatewayArnTmp[2];
  let resource = '/'; // root resource
  if (apiGatewayArnTmp[3]) {
    resource += apiGatewayArnTmp[3];
  }
  apiOptions.resource = resource;

  return apiOptions;
};

exports.getApiOptions = getApiOptions;
