const AWS = require('aws-sdk');
const R = require('ramda');
const logHelper = require('./logHelper');

const kms = new AWS.KMS({
  region: process.env.region || 'us-west-2'
});

let cachedEnvs;

const envToPromise = async (v, k) => {
  const data = await kms.decrypt({
    CiphertextBlob: Buffer.from(v, 'base64')
  }).promise();
  return R.pair(k, data.Plaintext.toString());
};

const updateCache = decryptedObject => {
  cachedEnvs = decryptedObject;

  return cachedEnvs;
};

const clearCache = () => {
  cachedEnvs = undefined;
};

const toDecryptionPromises = encryptedEnvKeys => R.pipe(
  R.pickBy((value, key) => R.contains(key, encryptedEnvKeys) && !R.isNil(value)),
  R.mapObjIndexed(envToPromise),
  R.values
);

const normalize = R.curry(async (encryptedEnvKeys, envs) => {
  if (!R.isNil(cachedEnvs)) {
    logHelper.log('kms_helper_normalize', { status: 'using cached envs' });
    return Promise.resolve(cachedEnvs);
  }

  if (R.isEmpty(encryptedEnvKeys)) {
    logHelper.log('kms_helper_normalize', { status: 'no encrypted envs, using as given' });
    updateCache(encryptedEnvKeys);
    return Promise.resolve(envs);
  }

  logHelper.log('kms_helper_normalize', { status: 'decrypting keys', keys: encryptedEnvKeys });
  const results = await Promise.all(toDecryptionPromises(encryptedEnvKeys)(envs));
  const decryptedObject = R.fromPairs(results);
  logHelper.log('kms_helper_normalize', { status: 'decrypted', keys: R.keys(decryptedObject) });
  const normalizedEnvs = Object.assign({}, envs, decryptedObject);
  return updateCache(normalizedEnvs);
});

/**
 * @param {object} value
 * @param {string} key to decrypt in value
 * @returns {object} clone of value with key decrypted
 */
const decrypt = async (value, key) => {
  const data = await kms.decrypt({ CiphertextBlob: Buffer.from(R.prop(key, value), 'base64') }).promise();
  return R.assoc(key, data.Plaintext.toString(), value);
};

exports.decrypt = decrypt;
exports.normalize = normalize;
exports.clearCache = clearCache;
