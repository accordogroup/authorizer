const jwt = require('jsonwebtoken');
const R = require('ramda');
const jwks = require('./jwksClient');
const logHelper = require('./logHelper');

const trimToken = tokenString => (tokenString.replace('bearer', '').trim());
const validateToken = (token, config) => {
  token = token.replace('bearer', ''); // In the wild, it'll come with a barer prefix
  let secret = config.secret;
  if (!secret) {
    throw new Error('No secret configured');
  }

  if (!config.verifyOptions) {
    throw new Error('No verify options configured');
  }

  // Don't ask. auth0 tokens need the buffer piece.
  if (!config.encodeSecret) {
    secret = Buffer.from(secret, 'base64');
  }

  return jwt.verify(token.trim(), secret, config.verifyOptions);
};

const decode = token => (jwt.decode(token, { complete: true }));

const eventTokenHasKid = event => (
  R.pathSatisfies(R.complement(R.isNil), ['header', 'kid'],
    decode(getEventToken(event)))
);

const getEnv = (aud, env) => {
  const acoClientIds = [
    'Af0stN16VXzUzBrPTtNB5MsU3vYUhWE2',
    '1h6oN06O2FIjQYiQoxiBJ8jppJOSnAB0',
    'PU6omSwOwTf1L98L1Ld9MlUyhKPRslWi'
  ];

  if (acoClientIds.includes(aud)) return `${env}_ACO`;
  else return env;
};

const verifyJWKS = async (token, jwtOptions, env) => {
  const trimmed = trimToken(token);
  const decoded = decode(trimmed);
  try {
    const key = await jwks.getEnvSigningKey(decoded.header.kid, getEnv(decoded.payload.aud, env));
    return jwt.verify(trimmed, key.publicKey || key.rsaPublicKey, jwtOptions);
  } catch (err) {
    logHelper.log('validateToken_verifyJWKS', err);
    throw err;
  }
};

const getEventToken = event => (trimToken(R.propOr('', 'authorizationToken', event)));
module.exports = {
  validateToken,
  decode,
  eventTokenHasKid,
  getEventToken,
  verifyJWKS
};
