const R = require('ramda');
const logHelper = require('@accordo-feed/webapi-helper').utils.logHelper;

/**
 * @param {object} the object to remove authorizationToken from
 */
const removeAuthorizationToken = R.evolve({
  authorizationToken: (token) => R.isNil(token) ? 'no token!' : '...a token...'
});

module.exports = {
  log: logHelper.log,
  removeAuthorizationToken
};
