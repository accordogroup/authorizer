const R = require('ramda');
const {
  ipifnyNamespace,
  optimizerNamespace
} = require('../constants');
const organizations = require('../gateways/organizations');

/**
 *
 * @param {string} token decoded auth0 token object
 * @param {string} stage of environment the access is being granted for
 * @returns {object} roles permitted to use { userRoles: [] orgRoles: [] }
 */
// ipifnyNamespace is for the new OIDC token
const getRoles = async (token, stage) => {
  const userRoles = R.uniq(
    R.pathOr([], ['app_metadata', 'roles'], token).concat(
      R.pathOr([], ['app_metadata', 'authorization', 'roles'], token),
      R.pathOr([], [`${ipifnyNamespace}app_metadata`, 'roles'], token),
      R.pathOr([], [`${ipifnyNamespace}app_metadata`, 'authorization', 'roles'], token)
    )
  );
  const orgRoles = R.uniq(
    R.pathOr([], ['app_metadata', 'organizations'], token)
      .concat(R.pathOr([], [`${ipifnyNamespace}app_metadata`, 'organizations'], token)
        .concat(R.pathOr([], [`${optimizerNamespace}organizations`], token))
        .concat(await generateMSPClientRoles(token, stage))
      )
  );
  return { userRoles, orgRoles };
};

const getUserId = (token) => {
  // Prod ones will be socialConnection|userid
  const tmp = token.sub.split('|');
  return tmp[tmp.length - 1];
};

/**
 * Note the organizations field is an array
 * We assume the first one is the active org as the array should be length 1
 * @param {object} token decoded auth0 token object
 * @returns {string} MSP id
 */
const getMSPId = R.pathOr('', [`${optimizerNamespace}organizations`, 0, 'orgId']);

/**
 *
 * @param {string} token decoded auth0 token object
 * @param {string} stage of environment the access is being granted for
 * @returns {object[]} orgs permitted to access [{ orgId, role: 'partner_admin'}]
 */
const generateMSPClientRoles = async (token, stage) => {
  const mspId = getMSPId(token);
  if (R.isEmpty(mspId)) return [];
  return R.uniq(R.map(
    orgId => ({ orgId, role: 'partner_admin' }),
    await organizations.getClientOrganisations(mspId, stage)
  ));
};

const unhelpful = (val) => R.or(R.isEmpty(val), R.isNil(val));

exports.unhelpful = unhelpful;
exports.getRoles = getRoles;
exports.getUserId = getUserId;
