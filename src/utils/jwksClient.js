const R = require('ramda');
const jwksClient = require('jwks-rsa');

const makeClient = R.memoizeWith(
  R.identity,
  env => (
    jwksClient({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 10, // default value
      jwksUri: process.env[`${env}_JWKS_URI`] || `no JWKS URI for ${env} found`
    })
  )
);

const getEnvSigningKey = async (keyId, env) => {
  const c = makeClient(env);
  if (!c) {
    throw new Error(`jwksClient: no client for env ${env}`);
  }
  return new Promise((resolve, reject) => {
    c.getSigningKey(keyId, (err, key) => {
      if (err) {
        return reject(err);
      }
      return resolve(key);
    });
  });
};

module.exports = {getEnvSigningKey};
