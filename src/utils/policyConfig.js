const logHelper = require('./logHelper');
const localPolicies = require('../gateways/local');
const s3 = require('../gateways/s3');

const policies = new Map();

const getVersion1Key = (restApiId, stage) => `${restApiId}/${stage}/config.json`;
const getVersion2Key = (restApiId) => `${restApiId}/config.json`;

/**
 * @param {object} options from the input with key fields
 * @param {string} restApiId the AWS api id to look up for
 * @param {string} stage the AWS api stage to look up v1 for
 * @returns {object} config for the api gateway (try to get v2, if cant then v1)
 */
const getPolicyConfig = async ({restApiId, stage}) => {
  const keys = [
    getVersion2Key(restApiId),
    getVersion1Key(restApiId, stage)
  ];
  let policyConfig;
  for (const key of keys) {
    try {
      logHelper.log('policyConfig_getPolicyConfig_key', key);
      policyConfig = policies.get(key) || await retrievePolicyFromStore(key);
      if (policyConfig) {
        logHelper.log('policyConfig_getPolicyConfig_found', key);
        policies.set(key, policyConfig);
        break;
      }
    } catch (err) {
      logHelper.log('policyConfig_getPolicyConfig', err);
    }
  }
  if (!policyConfig) throw new Error(`No potential configs found: ${JSON.stringify(keys)}`);
  return policyConfig;
};

const retrievePolicyFromStore = async (key) =>
  localPolicies.useLocal()
    ? await localPolicies.getConfigByKey(key)
    : await s3.getConfigByKey(key);

module.exports = {
  getPolicyConfig
};
