const R = require('ramda');

const requiredFields = ['methodArn', 'authorizationToken'];

const missingFields = (eventKeys) => R.reject(
  requiredField => R.find(R.equals(requiredField), eventKeys),
  requiredFields
);

const validate = (event) => {
  const keys = R.keys(event);
  return R.isEmpty(missingFields(keys));
};

const findMissingFields = (event) => {
  const keys = R.keys(event);
  return missingFields(keys);
};

module.exports = {
  validate,
  findMissingFields
};
