const fs = require('fs');
const util = require('util');
const path = require('path');
const logHelper = require('../utils/logHelper');

const readFileAsync = util.promisify(fs.readFile);

const LOCAL_FOLDER_PATH = path.resolve(__dirname, '../../policies/accordo.config/authorizer');

const useLocal = () => fs.existsSync(LOCAL_FOLDER_PATH);

/**
 * @param {string} key the unique path of the v1/v2 policy to look for
 * @returns {object} policy if found, otherwise undefined
 */
const getConfigByKey = async (key) => {
  logHelper.log('gateways_local_getConfigByKey', key);
  try {
    const config = await readFileAsync(`${LOCAL_FOLDER_PATH}/${key}`);
    return JSON.parse(config.toString());
  } catch (err) {
    if (err.message.includes('ENOENT: no such file or directory')) return;
    throw err;
  }
};

module.exports = {
  useLocal,
  getConfigByKey
};
