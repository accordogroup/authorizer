const AWS = require('aws-sdk');

const getLambdaService = region => new AWS.Lambda({
  region
});

const buildParams = (payload, stage) => ({
  FunctionName: `${stage}-organization-api`,
  InvocationType: 'RequestResponse',
  Payload: JSON.stringify(payload)
});

const invoke = async (payload, stage) => {
  const lambda = getLambdaService('us-west-2');
  return lambda.invoke(buildParams(payload, stage)).promise();
};

/**
 * @param {string} mspId
 * @param {string} stage of environment to find client orgs in
 * @returns {object[]} array of client org ids the mspId is permitted to access
 */
const getClientOrganisations = async (mspId, stage) => {
  const result = await invoke({
    action: 'GET_CLIENTS_BY_MSP_ID',
    body: {
      mspId
    }
  }, stage);
  const resultPayload = JSON.parse(result.Payload);
  const resultBody = JSON.parse(resultPayload.body);
  return (resultPayload.statusCode === 200)
    ? resultBody
    : [];
};

module.exports = {
  getClientOrganisations
};
