const AWS = require('aws-sdk');
const logHelper = require('../utils/logHelper');

const s3 = new AWS.S3({
  region: 'us-west-2'
});
const s3AuthorizerFolder = process.env['prefix'] || 'accordo-authorizer';

/**
 * @param {string} key the unique path of the v1/v2 policy to look for
 * @returns {object} policy if found, otherwise undefined
 */
const getConfigByKey = async (key) => {
  logHelper.log('gateways_s3_getPolicyConfig', key);
  try {
    const s3Params = { Bucket: 'accordo.config', Key: `${s3AuthorizerFolder}/${key}` };
    const data = await s3.getObject(s3Params).promise();
    return data ? JSON.parse(data.Body) : undefined;
  } catch (err) {
    if (err.message.includes('The specified key does not exist')) return;
    throw err;
  }
};

module.exports = {
  getConfigByKey
};
