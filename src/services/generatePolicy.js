/* eslint no-useless-escape:0 */
const R = require('ramda');
const AuthPolicy = require('aws-auth-policy');
const organizationPolicy = require('./organizationPolicy');
const userPolicy = require('./userPolicy');
const policyUtil = require('../utils/policyUtil');

const generatePolicy = async (token, apiOptions, appConfig) => {
  const policy = new AuthPolicy(token.sub, apiOptions.awsAccountId, apiOptions);
  policy.pathRegex = new RegExp('^[/.a-zA-Z0-9-\*_]+$');

  const userId = policyUtil.getUserId(token);
  const roles = await policyUtil.getRoles(token, apiOptions.stage);

  // Deny all methods if the user has no roles
  if (R.and(policyUtil.unhelpful(roles.userRoles), policyUtil.unhelpful(roles.orgRoles))) return denyAndBuild(policy);

  // Get all allowed methods
  const userMethods = userPolicy.getAllowedMethods(roles.userRoles, appConfig);
  const orgMethods = organizationPolicy.getAllowedMethods(roles.orgRoles, appConfig);

  // Deny all methods if couldn't find any allowed methods
  if (policyUtil.unhelpful(R.concat(userMethods, orgMethods))) return denyAndBuild(policy);

  // Collect User Permissions
  R.map((method) => {
    if (method.verb === '*' && method.resource === '*') { policy.allowAllMethods(); return; };
    policy.allowMethod(method.verb, R.replace('{sub}', userId, method.resource));
  }, userMethods);

  // Collect Org Permissions
  R.map((method) => { policy.allowMethod(method.verb, method.resource); }, orgMethods);
  return policy.build();
};

const denyAndBuild = (policy) => {
  policy.denyAllMethods();
  return policy.build();
};

exports.generatePolicy = generatePolicy;
