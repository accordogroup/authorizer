const R = require('ramda');
const policyUtil = require('../utils/policyUtil');
const logHelper = require('../utils/logHelper');

const getAllowedMethods = (userRoles, appConfig) => {
  if (policyUtil.unhelpful(userRoles)) { return []; }

  let allowedMethods = [];
  R.map((userRole) => {
    // filter the possible roles to that of the user
    const rolePolicies = R.filter(R.pathEq(['role'], userRole), appConfig.policies);
    if (R.isEmpty(rolePolicies)) {
      logHelper.log('userPolicy_getAllowedMethods_unknownRole', { userRole });
    }
    R.forEach((policy) => {
      allowedMethods = R.concat(allowedMethods, policy.resources);
    }, rolePolicies);
  }, userRoles);

  return allowedMethods;
};

exports.getAllowedMethods = getAllowedMethods;
