const R = require('ramda');
const policyUtil = require('../utils/policyUtil');

const getAllowedMethods = (orgRoles, appConfig) => {
  if (policyUtil.unhelpful(orgRoles)) { return []; }

  // Gets the first org policies that matches the current role
  const equalsRole = R.curry((resource, policy) => R.equals(resource.role, R.prop('orgRole', policy)));
  const filterOrgRoles = (role) => {
    const orgRoleMatch = R.find(equalsRole(role), appConfig.policies);
    return R.map(replaceOrgId(role), R.propOr([], 'resources', orgRoleMatch));
  };

  return R.flatten(R.map(filterOrgRoles, orgRoles));
};

const replaceOrgId = R.curry((role, resource) => {
  return R.merge(resource, { resource: R.replace(/{orgId}/g, role.orgId, resource.resource) });
});

exports.getAllowedMethods = getAllowedMethods;
