const R = require('ramda');
const jwt = require('../utils/validateToken');
const generatePolicy = require('../services/generatePolicy').generatePolicy;

const getClientVerifyOptions = (token, apiPolicy) =>
  R.propOr(null, 'verifyOptions', findClientMatch(apiPolicy.allowedClients, token));

const findClientMatch = (allowedClients, token) => R.find(
  client => audienceMatches(client, token) && issuerMatches(client, token),
  allowedClients
);

const audienceMatches = (client, token) => R.equals(
  R.path(['verifyOptions', 'audience'], client),
  R.path(['payload', 'aud'], token)
);

const issuerMatches = (client, token) => R.equals(
  R.path(['verifyOptions', 'issuer'], client),
  R.path(['payload', 'iss'], token)
);

/**
 * @param {object} payload (same as v2, just different token format)
 * { event: {
 *    type,
 *    authorizationToken,
 *    methodArn
 *  },
 *  options: { // the config appropriate for the api gateway called, see api-auth-policy repo
 *    dev: { allowedClients: [{ secret, verifyOptions, encodeSecret}] },
 *    uat: { allowedClients: [{ secret, verifyOptions, encodeSecret}] },
 *    prod: { allowedClients: [{ secret, verifyOptions, encodeSecret}] }
 *    policies: [ { role (or orgRole), resources } ],
 *    version: '2'
 * },
 * apiInfo: { // extracted from methodArn in event
 *  awsAccountId: 'test-account',
 *  region: 'test-region',
 *  restApiId: 'random-application',
 *  stage: 'dev',
 *  method: 'POST',
 *  resource: '/somethingNotMatter'
 * }
 * }
 * @returns {object} iam policy
 */
const authorizer = async (payload) => {
  console.log('%%%%%%% running v3');
  const token = jwt.getEventToken(payload.event);
  const decoded = jwt.decode(token);
  const verifyOptions = getClientVerifyOptions(decoded, payload.options[payload.apiInfo.stage]);
  if (R.isNil(verifyOptions)) {
    throw new Error(`authorizerV3: No verifyoptions for ${decoded.payload.aud}`);
  }
  const decodedToken = await jwt.verifyJWKS(token, verifyOptions, R.path(['apiInfo', 'stage'], payload));
  if (!decodedToken) throw new Error('Token not decoded');
  return await generatePolicy(decodedToken, payload.apiInfo, payload.options);
};

module.exports = { authorizerV3: authorizer };
