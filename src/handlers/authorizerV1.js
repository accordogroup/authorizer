const R = require('ramda');
const validateToken = require('../utils/validateToken').validateToken;
const generatePolicy = require('../services/generatePolicy').generatePolicy;
const ENCRYPTED_ENVS = require('../constants').ENCRYPTED_ENVS;
const normalize = require('../utils/kms_helper').normalize;

/**
 *
 * @param {object} payload
 * { event: {
 *    type,
 *    authorizationToken,
 *    methodArn
 *  },
 *  options: { // the config appropriate api gateway called, see api-auth-policy repo
 *    verifyOptions: {
 *      algorithm,
 *      issuer,
 *      audience
 *    }
 *    encodeSecret, // we get secret from env rather than policy
 *    policies: [ { role (or orgRole), resources } ]
 * },
 * apiInfo: { // extracted from methodArn in event
 *  awsAccountId: 'test-account',
 *  region: 'test-region',
 *  restApiId: 'random-application',
 *  stage: 'dev',
 *  method: 'POST',
 *  resource: '/somethingNotMatter'
 * }
 * }
 * @returns {object} iam policy
 */
const authorizer = async (payload) => {
  console.log('%%%%%%% running v1');
  const envs = await normalize(ENCRYPTED_ENVS, process.env);
  const config = R.merge(payload.options, {
    secret: envs[`${payload.apiInfo.stage}_secret`]
  });
  const decodedToken = validateToken(payload.event.authorizationToken, config);
  if (!decodedToken) throw new Error('Token not decoded');
  return await generatePolicy(decodedToken, payload.apiInfo, config);
};

module.exports = {
  authorizerV1: authorizer
};
