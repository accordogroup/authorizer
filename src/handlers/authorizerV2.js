const R = require('ramda');
const jwt = require('jsonwebtoken');
const validateToken = require('../utils/validateToken').validateToken;
const generatePolicy = require('../services/generatePolicy').generatePolicy;
const decrypt = require('../utils/kms_helper').decrypt;

/**
 *
 * @param {object} payload
 * { event: {
 *    type,
 *    authorizationToken,
 *    methodArn
 *  },
 *  options: { // the config appropriate for the api gateway called, see api-auth-policy repo
 *    dev: { allowedClients: [{ secret, verifyOptions, encodeSecret}] },
 *    uat: { allowedClients: [{ secret, verifyOptions, encodeSecret}] },
 *    prod: { allowedClients: [{ secret, verifyOptions, encodeSecret}] }
 *    policies: [ { role (or orgRole), resources } ],
 *    version: '2'
 * },
 * apiInfo: { // extracted from methodArn in event
 *  awsAccountId: 'test-account',
 *  region: 'test-region',
 *  restApiId: 'random-application',
 *  stage: 'dev',
 *  method: 'POST',
 *  resource: '/somethingNotMatter'
 * }
 * }
 * @returns {object} iam policy
 */
const authorizer = async (payload) => {
  console.log('%%%%%%% running v2');
  const stage = payload.apiInfo.stage;
  const config = R.merge(payload.options, { stage });
  const clientsWithSecrets = R.filter(R.has('secret'), config[stage].allowedClients);
  const decryptedClients = await Promise.all(R.map(R.curry(decrypt)(R.__, 'secret'), clientsWithSecrets));
  const token = payload.event.authorizationToken.replace('bearer', '').trim();
  const matchingClient = R.compose(R.equals(jwt.decode(token).aud), R.path(['verifyOptions', 'audience']));
  const validationConfig = R.find(matchingClient, decryptedClients);
  const decodedToken = validateToken(token, validationConfig);
  if (!decodedToken) throw new Error('Token not decoded');
  return await generatePolicy(decodedToken, payload.apiInfo, config);
};

module.exports = {authorizerV2: authorizer};
