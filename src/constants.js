exports.ENCRYPTED_ENVS = [
  'dev_secret',
  'qa_secret',
  'uat_secret',
  'prod_secret'
];

exports.SUPPORTED_STAGES = ['dev', 'qa', 'uat', 'prod'];

exports.ipifnyNamespace = 'https://accordo.io/';
exports.crmNamespace = 'https://crm.accordo.com/';
exports.optimizerNamespace = 'https://optimizer.accordo.com/';
