#!/bin/bash
set -e
echo "About to run smoketest against ${1}-authorizer"
export environment=${1}
npm run test:smoke
