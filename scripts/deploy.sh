# The script require following environment variables
# export BITBUCKET_BRANCH=master
# export BITBUCKET_COMMIT=1234567
# export admin_dev_secret=admin_dev_secret
# export admin_qa_secret=admin_qa_secret
# export admin_uat_secret=admin_uat_secret
# export admin_prod_secret=admin_prod_secret

#!/bin/bash
# relies on receiving authorizerType variable when called
# for serverless we need to have ${$1}CRON_SETTING present in env variables
set -e

authorizerType=$1

# branch and build info
export GIT_BRANCH=${BITBUCKET_BRANCH:-n/a}
export GIT_COMMIT=$(git rev-parse HEAD | cut -c1-7)

source ./scripts/tag.sh
export GIT_TAG=${BUILD_TAG:-n/a}


# debug
echo "about to deploy for '${authorizerType}' for tag '${GIT_TAG}' on commit '${GIT_COMMIT}'"

# serverless deploy
npm run deploy:authorizer -- --stage ${authorizerType} --force
# policy deploy
npm run deploy:policies -- ${authorizerType}